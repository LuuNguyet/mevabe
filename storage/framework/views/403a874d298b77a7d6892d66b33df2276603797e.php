<?php $__env->startSection('content'); ?>
<div class="breadcrumb-wrapper">
  <div class="breadcrumb-title">
    <h1 class="page-title"><span>Đăng Ký Tài Khoản</span></h1>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="/">Trang chủ</a></li>
        <li class="active"><a href="signin">Đăng Ký</a></li>
      </ul>
    </div>

  </div>
</div>
<div class="container">
  <div class="row">
    <div id="content" class="col-sm-12">
      <div class="position-display">
      </div>
      <p>Nếu bạn đã đăng ký tài khoản, vui lòng đăng nhập <a href="login">Tại Đây</a>.</p>
      <p> <b>Lưu ý:</b> Các mục dấu sao <b>màu đỏ</b> không được bỏ trống &amp; phải điền đầy đủ, chính xác</p>
      <form action="signin" method="post" enctype="multipart/form-data" class="form-horizontal">
        <?php echo csrf_field(); ?>
        <fieldset id="account">
          <legend>Thông tin cá nhân</legend>
            <?php if(!empty($action_result)): ?>
              <h3><strong style="color: red;"><?php echo e($action_result); ?></strong></h3>
            <?php endif; ?>

          <div class="form-group required" style="display: none;">
            <label class="col-sm-2 control-label">Customer Group</label>
            <div class="col-sm-10">
              <div class="radio">
                <label>
                  <input type="radio" name="customer_group_id" value="1" checked="checked">
                  Mặc định</label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-firstname">Tên:</label>
            <div class="col-sm-10">
              <input type="text" name="name" value="" placeholder="Tên:" id="input-firstname" class="form-control">
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-email">Địa chỉ E-Mail:</label>
            <div class="col-sm-10">
              <input type="email" name="email" value="" placeholder="Địa chỉ E-Mail:" id="input-email" class="form-control">
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-telephone">Điện Thoại:</label>
            <div class="col-sm-10">
              <input type="tel" name="phone" value="" placeholder="Điện Thoại:" id="input-telephone" class="form-control">
            </div>
          </div>
          <div class="form-group">
        <fieldset id="address">
          <legend>Địa chỉ của bạn</legend>
          <div class="form-group">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-address-1">Địa chỉ:</label>
            <div class="col-sm-10">
              <input type="text" name="address" value="" placeholder="Địa chỉ chính:" id="input-address-1" class="form-control">
            </div>
          </div>
        </fieldset>
        <fieldset>
          <legend>Mật khẩu</legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-password">Mật Khẩu:</label>
            <div class="col-sm-10">
              <input type="password" name="password" value="" placeholder="Mật Khẩu:" id="input-password" class="form-control">
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-confirm">Nhập lại Mật Khẩu:</label>
            <div class="col-sm-10">
              <input type="password" name="confirm" value="" placeholder="Nhập lại Mật Khẩu:" id="input-confirm" class="form-control">
            </div>
          </div>
        </fieldset>
        
        <div class="buttons">
          
            <input type="submit" value="Tiếp tục" class="btn btn-primary">
          
        </div>
      </form>
      <div class="position-display">
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>