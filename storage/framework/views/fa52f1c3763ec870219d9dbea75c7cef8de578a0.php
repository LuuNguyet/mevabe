<?php $__env->startSection('content'); ?>

    <body class="account-order-info">
    <div class="breadcrumb-wrapper">
        <div class="breadcrumb-title">
            <h1 class="page-title"><span>Thông tin đơn hàng</span></h1>
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="http://9736.chilishop.net/index.php?route=common/home">Trang chủ</a></li>
                    <li><a href="http://9736.chilishop.net/index.php?route=account/account">Tài khoản</a></li>
                    <li><a href="http://9736.chilishop.net/index.php?route=account/order">Lịch Sử Đặt Hàng</a></li>
                    <li class="active">
                        <a href="http://9736.chilishop.net/index.php?route=account/order/info&amp;order_id=29">Thông tin đơn hàng</a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-12">
                <div class="position-display"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-left" colspan="2">Chi tiết đơn hàng</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left" style="width: 50%;">
                                <?php $__currentLoopData = $order_id_created_at; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <b>Mã đơn hàng:</b> <?php echo e($item->order_id); ?><br>
                                    <b>Ngày tạo:</b> <?php echo e($item->created_at); ?>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </td>
                            <td class="text-left">
                                <b>Phương thức thanh toán:</b> Thu tiền khi giao hàng<br>
                                <b>Phương thức vận chuyển:</b> Phí vận chuyển cố định
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-left" style="width: 50%;">Thông tin tài khoản</td>
                            <td class="text-left">Địa chỉ giao hàng</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left">
                                <?php $__currentLoopData = $data_cus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php echo e($cus->name); ?><br>
                                    <?php echo e($cus->address); ?><br>
                                    <?php echo e($cus->phone); ?><br>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </td>
                            <td class="text-left">
                                <?php $__currentLoopData = $info_receiver; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $receiver): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php echo e($receiver->receiver_name); ?><br>
                                    <?php echo e($receiver->receiver_address); ?><br>
                                    <?php echo e($receiver->receiver_phone); ?><br>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-left">Tên sản phẩm</td>
                            <td class="text-left">Dòng sản phẩm</td>
                            <td class="text-right">Số lượng</td>
                            <td class="text-right">Đơn Giá</td>
                            <td class="text-right">Tổng Cộng</td>
                            <td style="width: 20px;"></td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $info_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td class="text-left"><?php echo e($product->title); ?><br>
                                &nbsp;<!--small> - Kích thước: </small-->
                            </td>
                            <td class="text-left"><?php echo e($product->collection_id); ?></td>
                            <td class="text-right"><?php echo e($product->quantity); ?></td>
                            <td class="text-right"><?php echo e($product->price); ?></td>
                            <td class="text-right"><?php echo e($product->price * $product->quantity); ?></td>
                            <td class="text-right" style="white-space: nowrap;">
                                <a href="http://9736.chilishop.net/index.php?route=account/order/reorder&amp;
                                order_id=29&amp;order_product_id=3645"
                                   data-toggle="tooltip"
                                   title="" class="btn btn-primary"
                                   data-original-title="Đặt hàng lại">
                                    <i class="fa fa-shopping-cart"></i></a>
                                <a href="http://9736.chilishop.net/index.php?route=account/return/add&amp;
                                order_id=29&amp;product_id=20" data-toggle="tooltip"
                                   title="" class="btn btn-danger"
                                   data-original-title="Đổi / Trả Hàng">
                                    <i class="fa fa-reply"></i></a>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right">
                                <b>Thành tiền</b>
                            </td>
                            <td class="text-right">
                                <?php if(Session::has('sum')): ?>
                                    <?php echo e(Session::get('sum')); ?>

                                <?php endif; ?>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right">
                                <b>Điểm thưởng():</b>
                            </td>
                            <td class="text-right"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right">
                                <b>Phí vận chuyển cố định</b>
                            </td>
                            <td class="text-right">
                                <?php if(Session::has('shipping_fee')): ?>
                                    <?php echo e(Session::get('shipping_fee')); ?>

                                <?php endif; ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right">
                                <b>Tổng cộng </b>
                            </td>
                            <td class="text-right">
                                <?php if(Session::has('total')): ?>
                                    <?php echo e(Session::get('total')); ?>

                                <?php endif; ?></td></td>
                            <td></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <h3>Lịch sử đơn hàng</h3>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-left">Ngày tạo</td>
                            <td class="text-left">Tình trạng</td>
                            <td class="text-left">Ghi chú</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left"></td>
                            <td class="text-left"></td>
                            <td class="text-left"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="buttons clearfix button-box">
                    <div class="pull-right">
                        <a href="http://9736.chilishop.net/index.php?route=account/order"
                           class="btn btn-primary">Tiếp tục</a></div>
                </div>
                <div class="position-display">
                </div></div>
        </div>
    </div>
    <div class="sticky-bottom">
        <div id="pcSupport" class="wrap">
            <a href="tel:19007179">Hotline 24/7: 1900 7179</a>
        </div>
    </div>
    <a href="tel:19007179" mypage="" class="call-now" rel="nofollow">
        <div class="mypage-alo-phone">
            <div class="animated infinite zoomIn mypage-alo-ph-circle"></div>
            <div class="animated infinite pulse mypage-alo-ph-circle-fill"></div>
            <div class="animated infinite tada mypage-alo-ph-img-circle"></div>
        </div>
    </a>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>