<?php $__env->startSection('content'); ?>
    <div class="breadcrumb-wrapper">
    <div class="breadcrumb-title">
        <h1 class="page-title"><span><?php echo e($page->title); ?></span></h1>
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Trang chủ</a></li>
                <li class="active"><a href="page?id=<?php echo e($page->id); ?>"><?php echo e($page->title); ?></a></li>
            </ul>
        </div>

    </div>
</div>
<div class="container">
    <div class="row">
        <div id="content" class="col-sm-12">
            <div class="position-display">
            </div>
            <?php echo e(htmlentities($page->content)); ?>

                <strong>CÔNG TY&nbsp;</strong><span
                        style="font-family: arial, helvetica, sans-serif; text-align: justify; font-weight: bold;">CHILDREN
                    TOYS</span></p>
            <p style="margin-top: 10px; margin-bottom: 15px; padding: 0px;  font-family: Arial, Helvetica, sans-serif;">
                <span style="font-size: small; font-family: arial, helvetica, sans-serif;"><strong>XƯỞNG SX ĐỒ CHƠI
                        VEETOYS</strong></span></p>
            <p style="margin-top: 10px; margin-bottom: 15px; padding: 0px;  font-family: Arial, Helvetica, sans-serif;">
                <span style="line-height: 18px; font-family: arial, helvetica, sans-serif;"></span></p>
            <p style="margin-top: 10px; margin-bottom: 15px; padding: 0px;  font-family: Arial, Helvetica, sans-serif;">
                <span style="font-size: small; font-family: arial, helvetica, sans-serif;">Số 123, Đường ABC, Quận ABC,
                    Thành Phố Hồ Chí Minh.</span></p>
            <div class="position-display">
            </div>
        </div>
    </div>
</div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>