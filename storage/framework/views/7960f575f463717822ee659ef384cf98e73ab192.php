<?php $__env->startSection('header'); ?> Trang nội dung
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="box box-info">
                <div class="form-horizontal form-product">
                    <div class="box-title clearfix">
                        <div class="pull-left title-tab">Tạo trang nội dung
                            <div class="action_result">
                                <span style="color: red;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Tựa trang<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12">
                                        <input name="name" placeholder="Tên trang nội dung"
                                               class="form-control title text-overflow-title">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Mô tả<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12">
                                        <textarea name="description" rows="8" cols="80" placeholder="Mô tả"
                                                  class="form-control title text-overflow-title"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Nội dung<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12">
                                        <textarea id="page_content" name="content" rows="8" cols="80"
                                                  placeholder="Mô tả"
                                                  class="tinymce form-control title text-overflow-title"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Trạng thái<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12">
                                        <input type="radio" name="status" value="active" checked="checked"><span class="label label-success">Sẵn sàng</span><br>
                                        <input type="radio" name="status" value="deleted"><span class="label label-danger">Bị xóa</span><br>
                                        <input type="radio" name="status" value="hiden"><span class="label label-warning">Bị ẩn</span><br>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-4 col-md-4 pull-right">
                                        <input type="button" id="page-create" value="Tạo"
                                               class="form-control title text-overflow-title btn btn-primary">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        tinymce.init({
            selector: 'textarea.tinymce',
            plugins: 'codesample'
        });
        $(document).ready(function () {
            $('#page-create').on('click', function () {
                try {
                    var name = $('input[name=name]').val();
                    var description = $('textarea[name=description]').val();
                    var content = tinymce.get('page_content').getContent();
                    var status = $('input[name=status]:checked').val();
                } catch (e) {
                    $('.action_result span').text('Hãy nhập đầy đủ thông tin');
                    window.scrollTo(document.body.scrollWidth, 0);
                    return;
                }
                if (name == "" || description == "" || content == "" || status == "") {
                    $('.action_result span').text('Hãy nhập đầy đủ thông tin');
                    window.scrollTo(document.body.scrollWidth, 0);
                    return;
                }
                var params = {
                    url: '/admin/createPage',
                    type: 'POST',
                    data: {
                        name: name,
                        description: description,
                        content: content,
                        status: status
                    },
                    success: function (result) {
                        alert('Thêm thành công nhóm sản phẩm');
                        window.location.href = '/admin/page_all';
                    }
                }
                $.ajax(params);
            })
        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>