<?php $__env->startSection('header'); ?>
Đăng nhập
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
  <div class="row">
     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="box box-info">
           <div class="form-horizontal form-product">
              <div class="box-title clearfix">
                 <div class="pull-left title-tab">Đăng nhập
                 </div>
              </div>
              <div class="box-body">
                 <div class="tab-content">
                   <div class="login-result" style="font-weight: 700; color: red;">
                   </div>
                    <div id="tab-vi" class="tab-pane fade in active">
                      <form action="" method="post">
                        <div class="form-group">
                          <div class="col-lg-2 col-md-12 control-label">Email<strong class="required">*</strong></div>
                          <div class="col-lg-10 col-md-12"><input name="email" placeholder="Email" class="form-control title text-overflow-title"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-lg-2 col-md-12 control-label">Password<strong class="required">*</strong></div>
                          <div class="col-lg-10 col-md-12"><input name="password" type="password" placeholder="Password" class="form-control title text-overflow-title"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-lg-4 col-md-4 pull-right"><input name="title" id="login-btn" type="submit" value="Đăng nhập" class="form-control title text-overflow-title"></div>
                        </div>
                      </form>
                       <script type="text/javascript">
                         $('#login-btn').on('click', function(e) {
                           e.preventDefault();
                           var email = $('input[name=email]').val();
                           var password = $('input[name=password]').val();
                           // console.log(email + "|"  + password);
                           var params = {
                             type: 'POST',
                             url: 'admin/login',
                             data: {
                               email: email,
                               password: password
                             },
                             success: function(result) {
                               result = $.parseJSON(result);
                               console.log(result);
                               if (result.code == 0) {
                                 window.location.href = 'admin';
                               } else {
                                 $('.login-result').text("Email hoặc mật khẩu không khớp!");
                               }
                             }
                           }
                           $.ajax(params);
                         })
                       </script>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>