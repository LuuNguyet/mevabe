<?php $__env->startSection('content'); ?> 
<div class="breadcrumb-wrapper">
    <div class="breadcrumb-title">
        <h1 class="page-title"><span><?php echo e($collection->title); ?></span></h1>
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/mevabe/public/">Trang chủ</a></li>
                <li class="active"><a href="collection?id=<?php echo e($collection->id); ?>"><?php echo e($collection->title); ?></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container categories-ms">
    <div class="row">
        <div id="column-left" class="col-sm-12 col-md-3 col-lg-3 col-xs-12">
            <div class="list-group menu_category hidden-xs hidden-sm">
                <div class="navbar-header">
                    <h3 id="category">Danh Mục</h3>
                    <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse"
                            data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
                </div>
                <?php $collections = Helper::getAllCollection(); //Collection left side with number of products
                ?>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav collapse in">
                        <?php $__currentLoopData = $collections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $collection_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="
                        <?php if($collection_item->id == $collection->id): ?>
                                    active
<?php endif; ?>
                                    "><a href="collection?id=<?php echo e($collection_item->id); ?>"
                                         class="active"> <?php echo e($collection_item->title); ?>

                                    (<?php echo e($collection_item->number_of_product); ?>) </a>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </ul>
                </div>
            </div>

            <script type="text/javascript">
                $(document).ready(function () {

                    $('.menu_category').find('li ul').toggleClass('collapse');
                    $('.menu_category').find('li').has('ul').children('.icon_menu').on('click', function () {
                        $(this).parent('li').toggleClass('open').children('ul').collapse('toggle');
                        $(this).parent('li').siblings().removeClass('open').children('ul.in').collapse('hide');

                    });

                    $('.menu_category').find('ul >li >a').hover(function () {
                        $(this).next('.icon_menu').toggleClass('color');
                    })

                    $('.menu_category li.active').parent('ul').addClass('collapse in');
                    $('.menu_category li li').not('.active').has('ul').children('ul').addClass('collapse');

                });
            </script>

            <div class="dv-builder-full">
                <div class="dv-builder list_product">
                    <div class="dv-module-content">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <div class="dv-item-module ">
                                    <div class="product_module special_product">
                                        <h3 class="title title_special">Khuyến mãi</h3>
                                        <?php $sale_products = Helper::getProducts(explode(",", Helper::getMeta('sale_products')));
                                        ?>
                                        <div class="product-layout-custom">
                                            <?php $__currentLoopData = $sale_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sale_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="medium">
                                                    <div class="product-thumb transition">
                                                        <div class="image"><a
                                                                    href="product?id=<?php echo e($sale_product->id); ?>"><img
                                                                        style="width: 60px; height: 75px;"
                                                                        src="uploads/<?php echo e($sale_product->featured_image); ?>"
                                                                        alt="<?php echo e($sale_product->title); ?>"
                                                                        title="<?php echo e($sale_product->title); ?>"
                                                                        class="img-responsive"></a>
                                                            <div class="button-group">
                                                                <button type="button" data-toggle="tooltip" title=""
                                                                        onclick="wishlist.add('55');"
                                                                        data-original-title="Yêu thích"><i
                                                                            class="fa fa-heart"></i>
                                                                    <span>Yêu thích</span>
                                                                </button>
                                                                <button type="button" data-toggle="tooltip" title=""
                                                                        onclick="compare.add('55');"
                                                                        data-original-title="So sánh"><i
                                                                            class="fa fa-exchange"></i>
                                                                    <span>So sánh</span>
                                                                </button>
                                                            </div>
                                                        </div>

                                                        <div class="caption">
                                                            <h4>
                                                                <a href="product?id=<?php echo e($sale_product->id); ?>"><?php echo e($sale_product->title); ?></a>
                                                            </h4>
                                                            <p class="description"><?php echo e($sale_product->description); ?></p>
                                                            <div class="rating">
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o"></i></span>
                                                                <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o"></i></span>
                                                            </div>
                                                            <p class="price">
                                                                <span class="price-old"><?php echo e($sale_product->price_compare); ?>

                                                                    VNĐ</span>
                                                                <br>
                                                                <span class="price-new"><?php echo e($sale_product->price); ?>

                                                                    VNĐ</span>
                                                            </p>
                                                        </div>
                                                        <div class="button-group-cart">
                                                            <button type="button" data-toggle="tooltip" title=""
                                                                    onclick="cart.add('55');"
                                                                    data-original-title="Thêm vào giỏ"><span>Thêm vào
                                                                    giỏ</span></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="button-group-cart">
                            <button type="button" data-toggle="tooltip" title="" onclick="cart.add('55');"
                                    data-original-title="Thêm vào giỏ"><span>Thêm vào giỏ</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="content" class="col-xs-12 col-sm-12 col-md-9 col-lg-9 category_page">
            <div class="position-display">
            </div>
            <div class="row hidden-xs">
            </div>
            <div class="page-selector">
                <div class="pages-box hidden-xs">
                    <a href="http://9736.chilishop.net/index.php?route=product/compare" id="compare-total">So sánh sản
                        phẩm (0)</a>
                </div>
                <div class="shop-grid-controls">
                    <div class="entry hidden-md hidden-sm hidden-xs">
                        <div class="inline-text">Sắp xếp:</div>
                        <div class="simple-drop-down">
                            <select id="input-sort" onchange="location = this.value;">
                                <option value="http://9736.chilishop.net/do-choi-go?sort=p.sort_order&amp;order=ASC"
                                        selected="selected">Mặc định
                                </option>
                                <option value="http://9736.chilishop.net/do-choi-go?sort=pd.name&amp;order=ASC">Tên (A -
                                    Z)
                                </option>
                                <option value="http://9736.chilishop.net/do-choi-go?sort=pd.name&amp;order=DESC">Tên (Z
                                    - A)
                                </option>
                                <option value="http://9736.chilishop.net/do-choi-go?sort=p.price&amp;order=ASC">Giá
                                    (Thấp &gt; Cao)
                                </option>
                                <option value="http://9736.chilishop.net/do-choi-go?sort=p.price&amp;order=DESC">Giá
                                    (Cao &gt; Thấp)
                                </option>
                                <option value="http://9736.chilishop.net/do-choi-go?sort=rating&amp;order=DESC">Đánh giá
                                    (Cao nhất)
                                </option>
                                <option value="http://9736.chilishop.net/do-choi-go?sort=rating&amp;order=ASC">Đánh giá
                                    (Thấp nhất)
                                </option>
                                <option value="http://9736.chilishop.net/do-choi-go?sort=p.model&amp;order=ASC">Kiểu (A
                                    - Z)
                                </option>
                                <option value="http://9736.chilishop.net/do-choi-go?sort=p.model&amp;order=DESC">Kiểu (Z
                                    - A)
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="entry hidden-md hidden-sm hidden-xs">
                        <button type="button" id="grid-view" class="view-button grid active" data-toggle="tooltip"
                                title="" data-original-title="Lưới"><i class="fa fa-th"></i></button>
                        <button type="button" id="list-view" class="view-button list" data-toggle="tooltip" title=""
                                data-original-title="Danh sách"><i class="fa fa-list"></i></button>
                    </div>
                    <div class="entry">
                        <div class="inline-text">Hiển thị:</div>
                        <div class="simple-drop-down display_number_c">
                            <select id="input-limit" onchange="location = this.value;">
                                <option value="http://9736.chilishop.net/do-choi-go?limit=12" selected="selected">12
                                </option>
                                <option value="http://9736.chilishop.net/do-choi-go?limit=25">25</option>
                                <option value="http://9736.chilishop.net/do-choi-go?limit=50">50</option>
                                <option value="http://9736.chilishop.net/do-choi-go?limit=75">75</option>
                                <option value="http://9736.chilishop.net/do-choi-go?limit=100">100</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="row">
                <?php $__currentLoopData = $collection->products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="product-thumb transition">
                            <div class="image"><a href="product?id=<?php echo e($product->id); ?>"><img
                                            src="uploads/<?php echo e($product->featured_image); ?>" alt="<?php echo e($product->title); ?>"
                                            title="<?php echo e($product->title); ?>" class="img-responsive"></a>
                                <div class="button-group">
                                    <button type="button" data-toggle="tooltip" title="" onclick="wishlist.add('55');"
                                            data-original-title="Yêu thích">
                                        <span>Yêu thích</span>
                                    </button>
                                    <button type="button" data-toggle="tooltip" title="" onclick="compare.add('55');"
                                            data-original-title="So sánh">
                                        <span>So sánh</span>
                                    </button>
                                </div>
                            </div>

                            <div class="caption">
                                <h4><a href="product?id=<?php echo e($product->id); ?>"><?php echo e($product->title); ?></a></h4>
                                <p class="description"><?php echo e($product->description); ?>}</p>
                                
                                <p class="price">
                                    <span class="price-old"><?php echo Helper::formatMoney($product->price_compare); ?> VNĐ</span>
                                    <br>
                                    <span class="price-new"><?php echo Helper::formatMoney($product->price_compare); ?> VNĐ</span>
                                </p>
                            </div>
                            <div class="position-display">
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
</div>
<div class="custom-bottom" id="custom_bottom">
    <div class="container">
        <column class="position-display">
            <div>
                <div class="dv-builder-full">
                    <div class="dv-builder line_ms">
                        <div class="dv-module-content">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                                    <div class="dv-item-module ">
                                        <div class="content_info follow-us">
                                            <ul class="list">
                                                <li class="item-content">
                                                    <div class="item-description">
                                                        Mạng xã hội:
                                                    </div>
                                                </li>
                                                <?php
                                                    $youtub = Helper::getMeta('youtube');
                                                ?>
                                                <li class="item-content">
                                                    <a href="<?php echo e($youtub); ?>" target="_blank">
                                                        <div class="item-image">
                                                            <i class="fa icon-youtube"></i>
                                                        </div>
                                                    </a>
                                                </li>
                                                <?php
                                                    $twit = Helper::getMeta('twitter');
                                                ?>
                                                <li class="item-content">
                                                    <a href="<?php echo e($twit); ?>" target="_blank">
                                                        <div class="item-image">
                                                            <i class="fa icon-twitter"></i>
                                                        </div>
                                                    </a>
                                                </li>
                                                <?php
                                                    $fb = Helper::getMeta('facebook');
                                                ?>
                                                <li class="item-content">
                                                    <a href="<?php echo e($fb); ?>" target="_blank">
                                                        <div class="item-image">
                                                            <i class="fa icon-facebook"></i>
                                                        </div>
                                                    </a>
                                                </li>
                                                <?php
                                                    $rss = Helper::getMeta('rss');
                                                ?>
                                                <li class="item-content">
                                                    <a href="<?php echo e($rss); ?>" target="_blank">
                                                        <div class="item-image">
                                                            <i class="fa icon-rss"></i>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                                    <div class="dv-item-module ">
                                        <div class="content_info text_ms checks">
                                            <ul class="list">
                                                <li class="item-content">
                                                    <?php
                                                        $service1 = Helper::getMeta('service_1');
                                                    ?>
                                                    <div class="item-description">
                                                        <?php echo e($service1); ?>

                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                                    <div class="dv-item-module ">
                                        <div class="content_info text_ms text_ms2">
                                            <ul class="list">
                                                <li class="item-content">
                                                    <?php
                                                        $service2 = Helper::getMeta('service_2');
                                                    ?>
                                                    <div class="item-title">
                                                        <?php echo e($service2); ?> <span class="title-child"></span>
                                                    </div>
                                                    <?php
                                                        $service3 = Helper::getMeta('service_3');
                                                    ?>
                                                    <div class="item-description">
                                                        <?php echo e($service3); ?>

                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="dv-builder-full">
                    <div class="dv-builder list_product">
                        <div class="dv-module-content">
                            <div class="row">
                                <div class="col-sm-12 col-md-3 col-lg-3 col-xs-12">
                                    <div class="dv-item-module ">
                                        <div class="product_module product_latest">
                                            <h3 class="title title_latest">Mới nhất</h3>
                                            <?php
                                                $ids = array_slice(explode(',', Helper::getMeta('new-products')), 0, 2);
                                                $new_products = Helper::getProducts($ids);
                                            ?>
                                            <div class="product-layout-custom">
                                                <?php $__currentLoopData = $new_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="medium">
                                                        <div class="product-thumb transition">
                                                            <div class="image">
                                                                <a
                                                                        href="/product?id=<?php echo e($product->id); ?>"><img
                                                                            src="/uploads/<?php echo e($product->featured_image); ?>"
                                                                            alt="<?php echo e($product->title); ?>"
                                                                            title="<?php echo e($product->title); ?>"
                                                                            class="img-responsive" style="width: 60px; height: 75px;"></a>

                                                            </div>
                                                            <div class="caption">
                                                                <h4>
                                                                    <a href="/product?id=<?php echo e($product->id); ?>"><?php echo e($product->title); ?></a>
                                                                </h4>
                                                                <p class="description"><?php echo e($product->description); ?></p>

                                                                <p class="price">
                                                                    <span class="price-old"><?php echo e(Helper::formatMoney($product->price)); ?>

                                                                        VNĐ</span>
                                                                    <br>
                                                                    <span class="price-new"><?php echo e(Helper::formatMoney($product->price_compare)); ?>

                                                                        VNĐ</span>
                                                                </p>
                                                            </div>
                                                            <div class="button-group-cart">
                                                                <button type="button" data-toggle="tooltip" title=""
                                                                        data-original-title="Thêm vào giỏ"><span>Thêm
                                                                        vào
                                                                        giỏ</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-3 col-lg-3 col-xs-12">
                                    <div class="dv-item-module ">
                                        <div class="product_module product_bestseller">
                                            <h3 class="title title_bestseller"><span>Bán chạy<span></span></span></h3>
                                            <?php
                                                $ids = array_slice(explode(',',Helper::getMeta('sale-products')), 0, 2);
                                                $sale_products = Helper::getProducts($ids);
                                            ?>
                                            <div class="product-layout-custom">
                                                <?php $__currentLoopData = $sale_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="medium">
                                                        <div class="product-thumb transition">
                                                            <div class="image">
                                                                <a
                                                                        href="/product?id=<?php echo e($product->id); ?>"><img
                                                                            src="/uploads/<?php echo e($product->featured_image); ?>"
                                                                            alt="<?php echo e($product->title); ?>"
                                                                            title="<?php echo e($product->title); ?>"
                                                                            class="img-responsive" style="width: 60px; height: 75px;"></a>

                                                            </div>
                                                            <div class="caption">
                                                                <h4>
                                                                    <a href="/product?id=<?php echo e($product->id); ?>"><?php echo e($product->title); ?></a>
                                                                </h4>
                                                                <p class="description"><?php echo e($product->description); ?></p>

                                                                <p class="price">
                                                                    <span class="price-old"><?php echo e(Helper::formatMoney($product->price)); ?>

                                                                        VNĐ</span>
                                                                    <br>
                                                                    <span class="price-new"><?php echo e(Helper::formatMoney($product->price_compare)); ?>

                                                                        VNĐ</span>
                                                                </p>
                                                            </div>
                                                            <div class="button-group-cart">
                                                                <button type="button" data-toggle="tooltip" title=""
                                                                        data-original-title="Thêm vào giỏ"><span>Thêm
                                                                        vào
                                                                        giỏ</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-3 col-lg-3 col-xs-12">
                                    <div class="dv-item-module ">
                                        <div class="product_module special_product">
                                            <h3 class="title title_special">Khuyến mãi</h3>
                                            <?php
                                                $ids = array_slice(explode(',', Helper::getMeta('promotion-products')), 0, 2);
                                                $promotion_products = Helper::getProducts($ids);
                                            ?>
                                            <div class="product-layout-custom">
                                                <?php $__currentLoopData = $promotion_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="medium">
                                                        <div class="product-thumb transition">
                                                            <div class="image">
                                                                <a
                                                                        href="/product?id=<?php echo e($product->id); ?>"><img
                                                                            src="/uploads/<?php echo e($product->featured_image); ?>"
                                                                            alt="<?php echo e($product->title); ?>"
                                                                            title="<?php echo e($product->title); ?>"
                                                                            class="img-responsive" style="width: 60px; height: 75px;"></a>

                                                            </div>
                                                            <div class="caption">
                                                                <h4>
                                                                    <a href="/product?id=<?php echo e($product->id); ?>"><?php echo e($product->title); ?></a>
                                                                </h4>
                                                                <p class="description"><?php echo e($product->description); ?></p>

                                                                <p class="price">
                                                                    <span class="price-old"><?php echo e(Helper::formatMoney($product->price)); ?>

                                                                        VNĐ</span>
                                                                    <br>
                                                                    <span class="price-new"><?php echo e(Helper::formatMoney($product->price_compare)); ?>

                                                                        VNĐ</span>
                                                                </p>
                                                            </div>
                                                            <div class="button-group-cart">
                                                                <button type="button" data-toggle="tooltip" title=""
                                                                        data-original-title="Thêm vào giỏ"><span>Thêm
                                                                        vào
                                                                        giỏ</span></button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-3 col-lg-3 col-xs-12">
                                    <div class="dv-item-module ">
                                        <div class="content_info photos-flickr">
                                            <div class="title">
                                                <h3><span>HÌNH ẢNH INSTARGRAM</span></h3>
                                            </div>
                                            <ul class="list">
                                                <li class="item-content">
                                                    <div class="item-image">
                                                        <img src="http://9736.chilishop.net/image/cache/catalog/banner/0635664408198490046-75x75.jpg">
                                                    </div>
                                                </li>
                                                <li class="item-content">
                                                    <div class="item-image">
                                                        <img src="http://9736.chilishop.net/image/cache/catalog/banner/0635666064112382505-75x75.jpg">
                                                    </div>
                                                </li>
                                                <li class="item-content">
                                                    <div class="item-image">
                                                        <img src="http://9736.chilishop.net/image/cache/catalog/banner/0635697175474516468-75x75.jpg">
                                                    </div>
                                                </li>
                                                <li class="item-content">
                                                    <div class="item-image">
                                                        <img src="http://9736.chilishop.net/image/cache/catalog/banner/0635701578765306357-75x75.jpg">
                                                    </div>
                                                </li>
                                                <li class="item-content">
                                                    <div class="item-image">
                                                        <img src="http://9736.chilishop.net/image/cache/catalog/banner/0635827619704125069-75x75.jpg">
                                                    </div>
                                                </li>
                                                <li class="item-content">
                                                    <div class="item-image">
                                                        <img src="http://9736.chilishop.net/image/cache/catalog/banner/cm_b41424-75x75.jpg">
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </column>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>