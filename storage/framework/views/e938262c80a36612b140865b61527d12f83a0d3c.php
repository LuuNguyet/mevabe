<?php $__env->startSection('content'); ?>
<div class="breadcrumb-wrapper">
    <div class="breadcrumb-title">
        <h1 class="page-title"><span>Đơn Hàng Của Bạn Đã Được Xử Lý!</span></h1>
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Trang chủ</a></li>
                <li><a href="/cart">Giỏ hàng</a></li>
                <li><a href="/checkout">Thanh toán</a></li>
                <li class="active"><a href="#">Thành công</a>
                </li>
            </ul>
        </div>

    </div>
</div>
<div class="container">
    <div class="row">
        <div id="content" class="col-sm-12">
            <div class="position-display">
            </div>
            <h1>Đơn Hàng Của Bạn Đã Được Xử Lý!</h1>
            <p>Đơn hàng của bạn đã được xử lý thành công!</p>
            <p>Nếu có bất kỳ thắc mắc gì, vui lòng chuyển cho <a
                        href="/contact">chúng tôi</a>.</p>
            <p>Cám ơn đã mua hàng tại cửa hàng online của chúng tôi!</p>
            <div class="buttons">
                <div class="pull-right"><a href="/"
                                           class="btn btn-primary">Tiếp tục</a></div>
            </div>
            <div class="position-display">
            </div>
        </div>
    </div>
</div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>