<header>
    <div class="top" id="top">
        <div class="container">
            <div class="row">
                <div class="line_top">
                    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 hidden-xs">
                        <column class="position-display">
                            <div>
                                <div class="content_info hotline">
                                    <ul class="list">
                                        <li class="item-content">
                                            <div class="item-image">
                                                <i class="fa fa fa fa-phone"></i>
                                            </div>
                                            <div class="item-description">
                                                <?php
                                                    $phone = Helper::getMeta('phone');
                                                ?>
                                                <?php echo e($phone); ?>

                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </column>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 text-right pull-right">
                        <div id="top-links" class="nav pull-right">
                            <ul class="list-inline">
                                <?php if(Session::has('login')): ?>
                                    <?php if(Session::get('login')==true): ?>
                                        <li class="dropdown account_ms"><a
                                                    href="/customer"
                                                    title="Đăng Nhập" class="dropdown-toggle">
                                                <span>Xin Chào: <?php echo e(Session::get('customer')->email); ?></span>
                                            </a>
                                        </li>
                                        <li class=""><a
                                                    href="logout">
                                                <span>|Đăng xuất</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <li class="dropdown account_ms"><a
                                                href="/login" title="Đăng Nhập"
                                                class="dropdown-toggle">
                                            <span>Đăng Nhập</span> <span
                                                    class="caret"></span></a>
                                    </li>
                                <?php endif; ?>
                                <li>
                                    <div>
                                        <form action="/mevabe/resources/index.php?route=common/language/language"
                                              method="post" enctype="multipart/form-data" id="language">
                                            <div class="btn-group">
                                                <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                                    <span class="text-language" style="margin-right:5px;">Ngôn
                                                        ngữ</span>
                                                    <img src="resources/views/image/flags/vn.png"
                                                         alt="Tiếng Việt" title="Tiếng Việt">
                                                    <i class="fa fa-caret-down"></i></button>

                                                <ul class="dropdown-menu">
                                                    <li><a href="vi"><img
                                                                    src="resources/views/image/flags/vn.png"
                                                                    alt="Tiếng Việt"
                                                                    title="Tiếng Việt"/> Tiếng Việt</a></li>
                                                </ul>
                                            </div>
                                            <input type="hidden" name="code" value=""/>
                                            <input type="hidden" name="redirect"
                                                   value="/mevabe/resources/index.php?route=common/home"/>
                                        </form>
                                    </div>
                                </li>
                                <li class="lisearch">
                                    <span class="switch-search">
                                        <a class="fa fa-search"></a>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="header" id="header">
        <div class="container menu-button responsive-menu-toggle-class">
            <i class="toggle-menu-control-open"></i>
        </div>
        <div class="container">
            <column class="position-display">
                <div>
                    <div class="dv-builder-full">
                        <div id='header' class="dv-builder header">
                            <div class="dv-module-content">
                                <div class="row">
                                    <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                                        <div class="dv-item-module ">
                                            <!-- <div id="search" class="input-group">
                                                <input type="text" name="search" value="" placeholder="Tìm kiếm sản phẩm" class="form-control input-lg" />
                                                <span class="input-group-btn">
                                                  <button type="button" class="btn btn-default btn-lg"></button>
                                                </span>
                                              </div> -->
                                            <div class="search-button">
                                                <div class="switch-search hidden-xs">
                                                    <a class="fa fa-search"></a>
                                                </div>
                                                <div class="search-box">
                                                    <div class="search-box2" id="search">
                                                        <span class="close-search"></span>
                                                        <h3>Tìm kiếm sản phẩm</h3>
                                                        <div class="marright">
                                                            <input type="text" name="search" value=""
                                                                   placeholder="Tìm kiếm sản phẩm"
                                                                   class="form-control input-lg"/>
                                                            <button type="button"
                                                                    class="btn btn-default btn-lg"></button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        $logo = Helper::getMeta('logo');
                                    ?> 
                                    <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                                        <div class="dv-item-module ">
                                            <div id="logo">
                                                <a href="/">
                                                    <img src="uploads/<?php echo e($logo); ?>"
                                                         title="Children Toys" alt="Children Toys"
                                                         class="img-responsive "/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                                        <div class="dv-item-module ">
                                            <div id="cart" class="btn-group btn-block">
                                                <?php
                                                    $cart_session = Helper::getCartFromSession()
                                                ?>
                                                <button type="button" data-toggle="dropdown"
                                                        data-loading-text="Đang Xử lý..."
                                                        href="/cart"
                                                        class="btn btn-inverse btn-block btn-lg dropdown-toggle">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    <span id="cart-total">
                                                        <span class="num_product"><?php if(!Session::has('cart')): ?>
                                                                0
                                                            <?php endif; ?>
                                                        </span>
                                                        <span class="text-cart">sản phẩm</span>
                                                        <span class="price"></span>
                                                    </span>
                                                </button>
                                                <a class="ico-sum"></a>
                                                <ul class="dropdown-menu pull-right">
                                                    <?php if(Session::has('cart')): ?>
                                                        <li class="item table-responsive">
                                                            <table class="table table-striped">
                                                                <tbody>
                                                                <?php $__currentLoopData = $cart_session->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <tr>
                                                                        <td class="text-center">
                                                                            <a href="product?id=<?php echo e($item->id); ?>"><img
                                                                                        src="uploads/<?php echo e($item->featured_image); ?>"
                                                                                        alt="<?php echo e($item->title); ?>"
                                                                                        title="<?php echo e($item->title); ?>"
                                                                                        class="img-thumbnail"></a>
                                                                        </td>
                                                                        <td class="text-left"><a
                                                                                    href="product?id=<?php echo e($item->id); ?>"><?php echo e($item->title); ?></a>
                                                                        </td>
                                                                        <td class="text-right">
                                                                            x <?php echo e($item->quantity); ?></td>
                                                                        <td class="text-right color-ms"><?php echo e(Helper::formatMoney($item->price)); ?>

                                                                            VNĐ
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <button type="button" title="Loại bỏ"
                                                                                    class="btn btn-danger btn-xs btn-remove" data-id="<?php echo e($item->id); ?>">
                                                                                <i class="fa fa-times"></i></button>
                                                                        </td>
                                                                    </tr>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                </tbody>
                                                            </table>
                                                        </li>
                                                        <li>
                                                            <div>
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td class="text-right"><strong>Thành
                                                                                tiền</strong></td>
                                                                        <td class="text-right color-ms"><?php echo e(Helper::formatMoney($cart_session->total)); ?>

                                                                            VNĐ
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-right"><strong>Sản phẩm tính
                                                                                thuế</strong></td>
                                                                        <td class="text-right color-ms">20.000 VNĐ</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-right"><strong>Tổng
                                                                                cộng </strong></td>
                                                                        <td class="text-right color-ms"><?php echo e(Helper::formatMoney($cart_session->total + 20000)); ?>

                                                                            VNĐ
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                <p class="text-right active"><a href="/cart"
                                                                                                class="btn-cart">Xem Giỏ
                                                                        Hàng</a>&nbsp;&nbsp;&nbsp;<a href="/checkout"
                                                                                                     class="btn-carttow">Thanh
                                                                        Toán</a></p>
                                                            </div>
                                                        </li>
                                                    <?php else: ?>
                                                        <li>
                                                            <p class="text-center">Giỏ Hàng đang trống!</p>
                                                        </li>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </column>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            StoreAPI.getCart(function (result) {
                var items = result.data.items;
                var count = 0;
                $.each(items, function (i, e) {
                    count += e.quantity;
                })
                $('.num_product').html(parseInt(count));
            })
        });
        $('.btn-remove').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            StoreAPI.removeItem(id, function (result) {
                console.dir(result);
                if (result.status === 'redirect') {
                    window.location.href = "cart";
                }
                alert("Xóa thành công!");
                location.reload();
            })
        })
    </script>
    <div class="main-menu" id="main-menu">
        <div class="double-border">
            <div class="container">
                <column class="position-display">
                    <div>
                        <div class="close-header-layer"></div>
                        <div class="menu_horizontal" id="menu_id_MB01">
                            <div class="navbar-header">
                                <div class="title_navigation"><i class="fa fa-bars"></i></div>
                            </div>
                            <nav class="menu">
                                <li class="vertical-menu-list">
                                    <a href="/">

                                        Trang chủ </a>
                                </li>

                                <li class="vertical-menu-list">
                                    <a href="/page">

                                        Giới thiệu </a>
                                </li>

                                <li>
                                    <a href="#" data-toggle="dropdown">

                                        Sản phẩm
                                    </a>
                                    <i class="icon_menu_main"></i>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="/mevabe/resources/do-choi-go" class="trigger down-mega-child2"
                                               aria-expanded="false">
                                                Đồ chơi gỗ </a>
                                            <i class="icon_menu_main"></i>
                                            <ul>
                                                <li>
                                                    <a href="/mevabe/resources/do-choi-mo-hinh">
                                                        Đồ chơi mô hình </a>
                                                </li>
                                                <li>
                                                    <a href="/mevabe/resources/gau-bong">
                                                        Gấu bông </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="/mevabe/resources/do-choi-lego">
                                                Đồ chơi lego </a>
                                        </li>
                                        <li>
                                            <a href="/mevabe/resources/thu-bong">
                                                Thú bông </a>
                                        </li>
                                        <li>
                                            <a href="/mevabe/resources/do-choi-tri-tue">
                                                Đồ chơi trí tuệ </a>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#" data-toggle="dropdown">

                                        Hướng dẫn
                                    </a>
                                    <i class="icon_menu_main"></i>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="/mevabe/resources/huong-dan-thanh-toan.html">
                                                Hướng dẫn thanh toán </a>
                                        </li>
                                        <li>
                                            <a href="/mevabe/resources/huong-dan-dat-hang.html">
                                                Hướng dẫn đặt hàng </a>
                                        </li>
                                        <li>
                                            <a href="/mevabe/resources/chuyen-khoan-ngan-hang.html">
                                                Chuyển khoản ngân hàng </a>
                                        </li>
                                        <li>
                                            <a href="/mevabe/resources/thanh-toan-quan-paypal.html">
                                                Thanh toán qua Paypal </a>
                                        </li>
                                        <li>
                                            <a href="/mevabe/resources/chinh-sach-rieng-tu.html">
                                                Chính sách riêng tư </a>
                                        </li>
                                        <li>
                                            <a href="/mevabe/resources/thanh-toan-quan-ngan-luong.html">
                                                Thanh toán qua Ngân Lượng </a>
                                        </li>
                                        <li>
                                            <a href="/mevabe/resources/thanh-toan-bao-kim.html">
                                                Thanh toán qua Bảo Kim </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="vertical-menu-list">
                                    <a href="/contact">Liên hệ </a>
                                </li>

                            </nav>

                        </div>
                        <script>
                            $(function () {
                                window.prettyPrint && prettyPrint()
                                $(document).on('click', '.navbar .dropdown-menu', function (e) {
                                    e.stopPropagation()
                                })
                            })
                        </script>
                    </div>
                </column>
            </div>
        </div>
    </div>
</header>
