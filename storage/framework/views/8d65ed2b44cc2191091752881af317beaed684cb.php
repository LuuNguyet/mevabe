<?php $__env->startSection('content'); ?>
<div class="breadcrumb-wrapper">
  <div class="breadcrumb-title">
    <h1 class="page-title"><span>Đăng nhập tài khoản</span></h1>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="/">Trang chủ</a></li>
        <li><a href="/customer">Tài khoản</a></li>
        <li class="active"><a href="#">Đăng nhập</a></li>
      </ul>
    </div>

  </div>
</div>
<div class="container">
  <div class="row">
    <div id="content" class="col-sm-12">
      <div class="position-display">
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="well">
            <h2>Khách hàng mới</h2>
            <p><strong>Đăng ký tài khoản</strong></p>
            <p>Bằng cách tạo tài khoản bạn có thể mua sắm nhanh hơn, cập nhật tình trạng đơn hàng, theo dõi những đơn hàng đã đặt và đặc biệt là sẽ được hưởng nhiều chương trình ưu đãi!</p>
            <a href="/signin" class="btn btn-primary">Tiếp tục</a></div>
        </div>
        <div class="col-sm-6">
          <div class="well">
            <h2>Khách hàng cũ</h2>
            <p><strong>Tôi là khách hàng cũ</strong></p>
            <?php if(isset($action_result)): ?>
              <p><strong style="color: red;"><?php echo e($action_result); ?></strong></p>
            <?php endif; ?>
            <form action="login" method="post" enctype="multipart/form-data">
              <?php echo e(csrf_field()); ?>

              <div class="form-group">
                <label class="control-label" for="input-email">Địa chỉ E-Mail:</label>
                <input type="text" name="email" value="" autocomplete="off" placeholder="Địa chỉ E-Mail:" id="input-email" class="form-control">
              </div>
              <div class="form-group">
                <label class="control-label" for="input-password">Mật khẩu:</label>
                <input type="password" name="password" value="" autocomplete="off" placeholder="Mật khẩu:" id="input-password" class="form-control">
                <a href="http://9736.chilishop.net/index.php?route=account/forgotten">Quên mật khẩu</a></div>
              <input type="submit" value="Đăng nhập" class="btn btn-primary">
            </form>
          </div>
        </div>
      </div>
      <div class="position-display">
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>