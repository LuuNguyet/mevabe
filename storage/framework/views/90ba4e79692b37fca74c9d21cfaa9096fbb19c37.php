<?php $__env->startSection('content'); ?>
<div class="custom-top" id="custom_slider">
   <div class="container">
      <column class="position-display">
         <div>
            <!-- START REVOLUTION SLIDER  fullwidth mode -->
            <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container"
               style="margin:0px auto;background-color:#E9E9E9;padding:0px;margin-top:0px;margin-bottom:0px;max-height:545px;">
               <div id="rev_slider_1_1" class="rev_slider fullwidthabanner"
                  style="display:none;max-height:545px;height:545px;">
                  <ul>
                     <?php
                     $slide_1 = Helper::getMeta('slide_1');
                     ?>
                     <!-- SLIDE  -->
                     <li data-transition="random" data-slotamount="7" data-masterspeed="300"
                        data-saveperformance="off">
                        <!-- MAIN IMAGE -->
                        <img src="uploads/<?php echo e($slide_1); ?>"
                           alt="Slider1" data-bgposition="center center" data-bgfit="cover"
                           data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                     </li>
                     <?php
                     $slide_2 = Helper::getMeta('slide_2');
                     ?>
                     <!-- SLIDE  -->
                     <li data-transition="random" data-slotamount="7" data-masterspeed="300"
                        data-saveperformance="off">
                        <!-- MAIN IMAGE -->
                        <img src="uploads/<?php echo e($slide_2); ?>"
                           alt="Slider2" data-bgposition="center center" data-bgfit="cover"
                           data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                     </li>
                     <?php
                     $slide_3 = Helper::getMeta('slide_3');
                     ?>
                     <!-- SLIDE  -->
                     <li data-transition="random" data-slotamount="7" data-masterspeed="300"
                        data-saveperformance="off">
                        <!-- MAIN IMAGE -->
                        <img src="uploads/<?php echo e($slide_3); ?>"
                           alt="Slider3" data-bgposition="center center" data-bgfit="cover"
                           data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                     </li>
                     <!-- SLIDE  -->
                     <?php
                     $slide_4 = Helper::getMeta('slide_4');
                     ?>
                     <li data-transition="random" data-slotamount="7" data-masterspeed="300"
                        data-saveperformance="off">
                        <!-- MAIN IMAGE -->
                        <img src="/uploads/<?php echo e($slide_4); ?>"
                           alt="Slider4" data-bgposition="center top" data-bgfit="cover"
                           data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                     </li>
                  </ul>
                  <div class="tp-bannertimer"></div>
               </div>
               <script type="text/javascript">
                  /******************************************
                   -    PREPARE PLACEHOLDER FOR SLIDER    -
                   ******************************************/


                  var setREVStartSize = function () {
                      var tpopt = new Object();
                      tpopt.startwidth = 1170;
                      tpopt.startheight = 545;
                      tpopt.container = jQuery('#rev_slider_1_1');
                      tpopt.fullScreen = "off";
                      tpopt.forceFullWidth = "off";

                      tpopt.container.closest(".rev_slider_wrapper").css({
                          height: tpopt.container.height()
                      });
                      tpopt.width = parseInt(tpopt.container.width(), 0);
                      tpopt.height = parseInt(tpopt.container.height(), 0);
                      tpopt.bw = tpopt.width / tpopt.startwidth;
                      tpopt.bh = tpopt.height / tpopt.startheight;
                      if (tpopt.bh > tpopt.bw) tpopt.bh = tpopt.bw;
                      if (tpopt.bh < tpopt.bw) tpopt.bw = tpopt.bh;
                      if (tpopt.bw < tpopt.bh) tpopt.bh = tpopt.bw;
                      if (tpopt.bh > 1) {
                          tpopt.bw = 1;
                          tpopt.bh = 1
                      }
                      if (tpopt.bw > 1) {
                          tpopt.bw = 1;
                          tpopt.bh = 1
                      }
                      tpopt.height = Math.round(tpopt.startheight * (tpopt.width / tpopt.startwidth));
                      if (tpopt.height > tpopt.startheight && tpopt.autoHeight != "on") tpopt.height = tpopt.startheight;
                      if (tpopt.fullScreen == "on") {
                          tpopt.height = tpopt.bw * tpopt.startheight;
                          var cow = tpopt.container.parent().width();
                          var coh = jQuery(window).height();
                          if (tpopt.fullScreenOffsetContainer != undefined) {
                              try {
                                  var offcontainers = tpopt.fullScreenOffsetContainer.split(",");
                                  jQuery.each(offcontainers, function (e, t) {
                                      coh = coh - jQuery(t).outerHeight(true);
                                      if (coh < tpopt.minFullScreenHeight) coh = tpopt.minFullScreenHeight
                                  })
                              } catch (e) {
                              }
                          }
                          tpopt.container.parent().height(coh);
                          tpopt.container.height(coh);
                          tpopt.container.closest(".rev_slider_wrapper").height(coh);
                          tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);
                          tpopt.container.css({
                              height: "100%"
                          });
                          tpopt.height = coh;
                      } else {
                          tpopt.container.height(tpopt.height);
                          tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);
                          tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);
                      }
                  };

                  /* CALL PLACEHOLDER */
                  setREVStartSize();


                  var tpj = jQuery;

                  var revapi1;

                  tpj(document).ready(function () {

                      if (tpj('#rev_slider_1_1').revolution == undefined)
                          revslider_showDoubleJqueryError('#rev_slider_1_1');
                      else
                          revapi1 = tpj('#rev_slider_1_1').show().revolution({
                              dottedOverlay: "none",
                              delay: 9000,
                              startwidth: 1170,
                              startheight: 545,
                              hideThumbs: 200,

                              thumbWidth: 100,
                              thumbHeight: 50,
                              thumbAmount: 4,


                              simplifyAll: "off",

                              navigationType: "bullet",
                              navigationArrows: "solo",
                              navigationStyle: "round",

                              touchenabled: "on",
                              onHoverStop: "on",
                              nextSlideOnWindowFocus: "off",

                              swipe_threshold: 75,
                              swipe_min_touches: 1,
                              drag_block_vertical: false,


                              keyboardNavigation: "off",

                              navigationHAlign: "center",
                              navigationVAlign: "bottom",
                              navigationHOffset: 0,
                              navigationVOffset: 20,

                              soloArrowLeftHalign: "left",
                              soloArrowLeftValign: "center",
                              soloArrowLeftHOffset: 20,
                              soloArrowLeftVOffset: 0,

                              soloArrowRightHalign: "right",
                              soloArrowRightValign: "center",
                              soloArrowRightHOffset: 20,
                              soloArrowRightVOffset: 0,

                              shadow: 0,
                              fullWidth: "on",
                              fullScreen: "off",

                              spinner: "spinner0",

                              stopLoop: "off",
                              stopAfterLoops: -1,
                              stopAtSlide: -1,

                              shuffle: "off",

                              autoHeight: "off",
                              forceFullWidth: "off",


                              hideThumbsOnMobile: "off",
                              hideNavDelayOnMobile: 1500,
                              hideBulletsOnMobile: "off",
                              hideArrowsOnMobile: "off",
                              hideThumbsUnderResolution: 0,

                              hideSliderAtLimit: 0,
                              hideCaptionAtLimit: 0,
                              hideAllCaptionAtLilmit: 0,
                              startWithSlide: 0
                          });


                  });
                  /*ready*/
               </script>
            </div>
            <!-- END REVOLUTION SLIDER -->
         </div>
      </column>
   </div>
</div>
<div class="container" id="content">
   <div class="row">
      <div class="col-sm-12">
         <div class="position-display">
            <div class="dv-builder-full">
               <div class="dv-builder hidden-xs">
                  <div class="dv-module-content">
                     <div class="row">
                        <?php
                        $ids = array_slice(explode(',', Helper::getMeta('desktop-colllections')), 0, 3);
                        $desktop_collection= Helper::getCollections($ids);
                        ?>
                        <?php $__currentLoopData = $desktop_collection; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $desktop_collection_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                           <div class="dv-item-module ">
                              <div id="banner0" class="banner_main hidden-xs">
                                 <div class="owl-wrapper-outer">
                                    <div class="item">
                                       <a href="collection?id=<?php echo e($desktop_collection_item->id); ?>"
                                          target="_blank"><img
                                          src="/uploads/<?php echo e($desktop_collection_item->image); ?>"
                                          alt="<?php echo e($desktop_collection_item->title); ?>"
                                          class="img-responsive" style="width: 370px; height: 240px;"></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </div>
                  </div>
               </div>
               <?php
               $ids = array_slice(explode(',', Helper::getMeta('hot-products')), 0, 4);
               $hot_products = Helper::getProducts($ids);
               ?>
               <div class="product_module product_featured">
                  <h3 class="title title_featured"><span>Sản phẩm nổi bật</span>
                  </h3>
                  <div class="row product-layout-custom">
                     <?php $__currentLoopData = $hot_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hot_product_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <div class="medium col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-thumb transition">
                           <div class="image">
                              <a
                                 href="/product?id=<?php echo e($hot_product_item->id); ?>">
                              <img
                                 src="/uploads/<?php echo e($hot_product_item->featured_image); ?>"
                                 alt="<?php echo e($hot_product_item->title); ?>"
                                 title="<?php echo e($hot_product_item->title); ?>"
                                 class="img-responsive"/>
                              </a>

                           </div>
                           <div class="caption">
                              <h4>
                                 <a href="/product?id=<?php echo e($hot_product_item->id); ?>"><?php echo e($hot_product_item->title); ?></a>
                              </h4>
                              <p class="description"><?php echo e($hot_product_item->description); ?></p>
                              <p class="price">
                                 <span class="price-old"><?php echo e(Helper::formatMoney($hot_product_item->price)); ?>

                                 VNĐ</span>
                                 </br>
                                 <span class="price-new"><?php echo e(Helper::formatMoney($hot_product_item->price_compare)); ?>

                                 VNĐ</span>
                              </p>
                           </div>
                           <div class="button-group-cart">
                              <button type="button" data-toggle="tooltip" title=""
                                 data-original-title="Thêm vào giỏ" data-id="<?php echo e($hot_product_item->id); ?>">
                              <span>Thêm vào giỏ</span>
                              </button>
                           </div>
                        </div>
                     </div>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </div>
               </div>
               <div class="position-display">
                  <div class="content_info banner_small">
                     <ul class="list">
                        <li class="item-content">
                           <div class="item-title">
                              Đồ chơi trẻ em là một trong những yếu tố hình thành nên tính cách của
                              bé. <span class="title-child"></span>
                           </div>
                           <div class="item-description">
                              Bạn nên chọn những đồ chơi giúp bé biết ý thức được những công việc bé
                              làm
                           </div>
                        </li>
                        <li class="item-content">
                           <div class="item-description">
                              Đó là các vấn đề luôn được các mẹ quan tâm.
                           </div>
                        </li>
                        <li class="item-content">
                           <div class="item-description">
                              Các loại đồ chơi trẻ em giúp các mẹ lựa chọn những món đồ chơi thích hợp
                              với độ tuổi cũng như tính cách của bé.
                           </div>
                        </li>
                        <li class="item-content">
                           <a href="/page" TARGET="_blank">
                              <div class="item-description">
                                 GIỚI THIỆU
                              </div>
                           </a>
                        </li>
                     </ul>
                  </div>
               </div>
               <?php echo $__env->make('..snippets/custom_bottom', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>