<?php $__env->startSection('content'); ?>
<div class="breadcrumb-wrapper">
  <div class="breadcrumb-title">
    <h1 class="page-title"><span><?php echo e($product->title); ?></span></h1>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="/">Trang chủ</a></li>
        <?php if(!empty($product->collection)): ?>
        <li><a href="collection?id=<?php echo e($product->collection->id); ?>"><?php echo e($product->collection->id); ?></a></li>
        <?php endif; ?>
        <li class="active"><a href="product?id=<?php echo e($product->id); ?>"><?php echo e($product->title); ?></a></li>
      </ul>
    </div>

  </div>
</div>
<div class="container product-ms">
  <div class="row item_success">
    <!--dont delete -> this is alert success ad product cart -->
  </div>
  <div class="row">
    <div id="content" class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
      <div class="position-display">
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="single-product-image">
            <div class="single-pro-main-image">
              <a href="product?id=<?php echo e($product->id); ?>" title="Thỏ bông thân thiện"><img id="optima_zoom" src="uploads/<?php echo e($product->featured_image); ?>" data-zoom-image="uploads/<?php echo e($product->featured_image); ?>" title="<?php echo e($product->title); ?>" alt="<?php echo e($product->title); ?>" class="img-responsive" /></a>
            </div>
            <div class="single-pro-thumb">
              <ul class="thubm-caro" id="optima_gallery">
                <li>
                  <a href="product?id=<?php echo e($product->id); ?>" title="<?php echo e($product->title); ?>" data-image="uploads/<?php echo e($product->featured_image); ?>" data-zoom-image="uploads/<?php echo e($product->featured_image); ?>"> <img class="img-responsive" src="uploads/<?php echo e($product->featured_image); ?>" title="<?php echo e($product->title); ?>" alt="<?php echo e($product->title); ?>" /> </a>
                </li>
                <?php if(!empty($product->images)): ?>
                  <?php $__currentLoopData = $product->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li>
                      <a href="product?id=<?php echo e($product->id); ?>" title="<?php echo e($product->title); ?>" data-image="uploads/<?php echo e($image->name); ?>" data-zoom-image="uploads/<?php echo e($image->name); ?>"> <img class="img-responsive" src="uploads/<?php echo e($image->name); ?>" title="<?php echo e($product->title); ?>" alt="<?php echo e($product->title); ?>" /> </a>
                    </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="single-product-description">
            <div class="pro-desc">
              <h2><strong><?php echo e($product->title); ?></strong></h2>
              <ul class="list-unstyled">
                <li>Mã sản phẩm: <span class="span"><?php echo e($product->id); ?></span></li>
                <li>Số lượng sản phẩm trong kho: <span class="span"><?php echo e($product->stock_quant); ?></span>
                </li>
              </ul>
              <ul class="list-unstyled des2">
                <li class="span">Mô tả</li>
                <li><?php echo $product->description; ?></li>
              </ul>
            </div>
            <!--  <div class="pro-desc">-->
            <ul class="list-unstyled">

              <li><span style="text-decoration: line-through;" class="old-price"><?php echo e(Helper::formatMoney($product->price_compare)); ?> VNĐ</span></li>
              <li>
                <span class="regular-price span_red"><?php echo e(Helper::formatMoney($product->price)); ?> VNĐ</span>
              </li>

            </ul>
          </div>
          <div id="product">
            <form action="addToCart" method="post">

              <?php echo csrf_field(); ?>
              <h3>Số lượng</h3> 
              <div class="product_details_cart">
                <div class="product-quantity">
                  <div class="numbers-row">
                    <input type="text" name="quantity" value="1" id="product_quantity">
                    <input type="hidden" name="product_id" value="56">
                    <div class="dec qtybutton btn-minus">-</div>
                    <div class="inc qtybutton btn-plus">+</div>
                  </div>
                  <script type="text/javascript">
                  $('.btn-minus').on('click', function(e) {
                    e.preventDefault();
                    if ($(this).parent().find('input[name=quantity]').val() > 1)
                    $(this).parent().find('input[name=quantity]').val($(this).parent().find('input[name=quantity]').val() - 1);
                    $(this).parent().find('input[name=quantity]').trigger('change');
                  });
                  $('.btn-plus').on('click', function(e) {
                    e.preventDefault();
                    $(this).parent().find('input[name=quantity]').val(parseInt($(this).parent().find('input[name=quantity]').val()) + 1);
                    $(this).parent().find('input[name=quantity]').trigger('change');
                  });
                  </script>

                  
                </div>
                <div class="single-poraduct-botton">
                  <button type="submit" data-loading-text="Đang Xử lý..." data-id="<?php echo e($product->id); ?>" class="add-btn has-quantity push_button button shopng-btn">Thêm vào giỏ</button>
                </div>
              </div>
            </form>
            <div class="product-quantity">
              <!-- AddThis Button BEGIN -->
              <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
              <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
              <!-- AddThis Button END -->
            </div>

            
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="bg-ms-product">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab-description" data-toggle="tab">Mô tả</a></li>
              
            </ul>
            <div class="tab-content">
              <div class="tab-pane active bottom20" id="tab-description">
               <?php echo $product->content; ?>

              </div>

            </div>
          </div>
        </div>
      </div>

    </div>
    <div id="column-right" class="col-sm-12 col-md-3 col-lg-3 col-xs-12">
      <div class="dv-builder-full">
        <div class="dv-builder list_product">
          <div class="dv-module-content">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                <div class="dv-item-module ">
                  <div class="product_module special_product">
                    <h3 class="title title_special">Khuyến mãi</h3>
                    <div class="product-layout-custom">
                      <?php $sale_products = Helper::getProducts(explode(",", Helper::getMeta('sale_products')));
                      ?>
                      <?php if(!empty($sale_products)): ?>
                      <?php $__currentLoopData = $sale_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sale_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <div class="medium">
                        <div class="product-thumb transition">
                          <div class="image"><a href="product?id=<?php echo e($sale_product->id); ?>"><img
                                                                            style="width: 60px; height: 75px;"
                                                                            src="uploads/<?php echo e($sale_product->featured_image); ?>"
                                                                            alt="<?php echo e($sale_product->featured_image); ?>"
                                                                            title="<?php echo e($sale_product->featured_image); ?>"
                                                                            class="img-responsive"></a> 
                          </div>

                          <div class="caption">
                            <h4>
                                                                    <a href="product?id=<?php echo e($sale_product->id); ?>"><?php echo e($sale_product->title); ?></a>
                                                                </h4>
                            <p class="description"><?php echo e($sale_product->description); ?></p>
                            <div class="rating">
                              <span class="fa fa-stack"><i
                                                                                class="fa fa-star-o"></i></span>
                              <span class="fa fa-stack"><i
                                                                                class="fa fa-star-o"></i></span>
                              <span class="fa fa-stack"><i
                                                                                class="fa fa-star-o"></i></span>
                              <span class="fa fa-stack"><i
                                                                                class="fa fa-star-o"></i></span>
                              <span class="fa fa-stack"><i
                                                                                class="fa fa-star-o"></i></span>
                            </div>
                            <p class="price">
                              <span class="price-old"><?php echo e(Helper::formatMoney($sale_product->price_compare)); ?>

                                                                        VNĐ</span>
                              <br>
                              <span class="price-new"><?php echo e(Helper::formatMoney($sale_product->price)); ?>

                                                                        VNĐ</span>
                            </p>
                          </div>
                        </div>
                      </div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <style>
  </style>
  <div class="product-late-ms product_module">
    <div class="row">
      <div class="col-sm-12">
        <div class="area-title">
          <h3 class="text_related title"><span>Sản phẩm liên quan</span></h3>
          <div class="titleborderOut">
            <div class="titleborder"></div>
          </div>
        </div>
      </div>
      <div class="featured-item">
        <?php if(!empty($product->related_products)): ?>
        <?php $__currentLoopData = $product->related_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $related_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="medium">
          <div class="product-thumb transition">
            <div class="image"><a href="product?id=<?php echo e($related_product->id); ?>"><img
                                                src="uploads/<?php echo e($related_product->featured_image); ?>"
                                                alt="<?php echo e($related_product->title); ?>" title="<?php echo e($related_product->title); ?>"
                                                class="img-responsive"/></a> 
            </div>

            <div class="caption">
              <h4><a href="product?id=<?php echo e($related_product->id); ?>"><?php echo e($related_product->title); ?></a>
                                    </h4>
              <p class="description"><?php echo e($related_product->description); ?></p>
              
              <p class="price">
                <span class="price-old"><?php echo e(Helper::formatMoney($related_product->price_compare)); ?> VNĐ</span>
                </br>
                <span class="price-new"><?php echo e(Helper::formatMoney($related_product->price)); ?> VNĐ</span>

              </p>
            </div>
            <div class="button-group-cart">
              <button type="button" data-id="<?php echo e($related_product->id); ?>" data-toggle="tooltip" title="Thêm vào giỏ"><span>Thêm vào giỏ</span></button>
            </div>
          </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<script>
  $('.add-btn').on('click', function(e) {
    e.preventDefault();
    var quantity = 1;
    var id = $(this).data('id');
    if($(this).hasClass("has-quantity")) {
      quantity = $('[name=quantity]').val();
    }
    StoreAPI.addItem(id, quantity, function(result) {
        // console.dir(result);
        alert("Thêm thành công " + quantity + " sản phẩm vào giỏ hàng!");
      setTimeout(function () {
          StoreAPI.getCart(function (result) {
              var items = result.data.items;
              var count = 0;
              $.each(items, function (i, e) {
                  count += e.quantity;
              })
              $('.num_product').html(parseInt(count));
          })
      }, 1000);
        location.reload();
    });
  })
  $(document).ready(function() {
      $('.button-group-cart button').on('click', function () {
          id = $(this).attr("data-id");
          window.location.href = "/product?id=" + id;
      })
    /*---- -----*/
    $('.thubm-caro').owlCarousel({
      items: 4,
      pagination: false,
      navigation: true,
      autoPlay: false,
      itemsDesktop: [1199, 3],
      itemsDesktopSmall: [991, 3],
      itemsTablet: [767, 3],
      itemsMobile: [480, 2],
      navigationText: ['<i class="fa fa-angle-left owl-prev-icon"></i>', '<i class="fa fa-angle-right owl-next-icon"></i>']
    });
    $('.product-late-ms .featured-item').owlCarousel({
      navigation: true,
      pagination: false,
      items: 4,
      itemsDesktop: [1199, 3],
      itemsDesktopSmall: [979, 3],
      itemsTablet: [767, 2],
      itemsMobile: [480, 1],
      navigationText: ['<div class="slide_arrow_prev"><i class="fa fa-angle-left"></i></div>', '<div class="slide_arrow_next"><i class="fa fa-angle-right"></i></div>']
    });
  })
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>