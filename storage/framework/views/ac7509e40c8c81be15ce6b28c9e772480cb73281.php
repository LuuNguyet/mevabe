<?php $__env->startSection('header'); ?>  Sản phẩm
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="box box-info">
                <div class="form-horizontal form-product">
                    <div class="box-title clearfix">
                        <div class="pull-left title-tab"><?php echo e($product->title); ?> | Lượt xem: <?php echo e($product->view); ?>

                            <div class="action_result">
                                <span style="color: red;"></span>
                            </div>
                        </div>
                        <div class="pull-right title-tab">
                            <a href="http://localhost:9000/product?id=<?php echo e($product->id); ?>" target="_blank"
                               class="next-preview">Xem trên website<i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Tên sản phẩm<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12">
                                        <input name="name" placeholder="Tên sản phẩm"
                                               value="<?php echo e($product->title); ?>"
                                               class="form-control title text-overflow-title">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Mô tả<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12">
                                        <textarea name="description" rows="8" cols="80" placeholder="Mô tả"
                                                  class="form-control title text-overflow-title"><?php echo e($product->description); ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Nội dung<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12">
                                        <textarea id="product_content" name="content" rows="8" cols="80"
                                                  placeholder="Nội dung"
                                                  class="tinymce form-control title text-overflow-title">
                                            <?php echo e($product->content); ?>

                                        </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Giá bán<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12"><input type="text" min="0" name="product_price"
                                                                            placeholder="Giá bán"
                                                                            value="<?php echo e($product->price); ?>"
                                                                            class="form-control title formatMoney">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Giá so sánh<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12"><input type="text" min="0"
                                                                            name="product_price_compare"
                                                                            placeholder="Giá so sánh"
                                                                            value="<?php echo e($product->price_compare); ?>"
                                                                            class="form-control title formatMoney">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-4 col-md-4 pull-right">
                                        <input type="button" id="product-update" value="Lưu"
                                               class="form-control title text-overflow-title btn btn-primary">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="pull-left title-tab">Quản lý tồn kho</div>
                    </div>
                    <div class="box-body">
                        <div class=""><input type="text" min="0" name="stock_manage"
                                             placeholder="Số lượng"
                                             value="<?php echo e($product->stock_quant); ?>"
                                             class="form-control title">
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="pull-left title-tab">Nhóm sản phẩm</div>
                    </div>
                    <div class="box-body">
                        <select multiple="multiple" class="multiple-select settings" name="special-collection">
                            <?php $__currentLoopData = Helper::getAllCollection(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $collection): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($collection->id); ?>"><?php echo e($collection->title); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="pull-left title-tab">Hình đại diện</div>
                    </div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="form-group">
                                <div class="col-lg-4 col-md-12">
                                    <div class="image-preview" data-key="<?php echo e($product->id); ?>" data-type="product" name="image">
                                        <label for="image-upload" class="image-label">Chọn hình</label>
                                        <input type="file" name="image" class="image-upload"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="pull-left title-tab">Trạng thái</div>
                    </div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="form-group">
                                <input type="hidden" name="id" value="<?php echo e($product->id); ?>">
                                <div class="col-lg-6 col-md-12">Trạng thái:<br>
                                    <input type="radio" name="status" value="active"
                                           <?php if($collection->status == 'active'): ?>
                                           checked=true
                                            <?php endif; ?>
                                    ><span class="label label-success">Sẵn sàng</span><br>
                                    <input type="radio" name="status" value="deleted"
                                           <?php if($collection->status == 'deleted'): ?>
                                           checked=true
                                            <?php endif; ?>
                                    ><span class="label label-danger">Bị xóa</span><br>
                                    <input type="radio" name="status" value="hiden"
                                           <?php if($collection->status == 'hiden'): ?>
                                           checked=true
                                            <?php endif; ?>
                                    ><span class="label label-warning">Bị ẩn</span><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<script type="text/javascript">
    tinymce.init({
        selector: 'textarea.tinymce',
        plugins: 'codesample'
    });
    $(document).ready(function() {
        $.uploadPreview({
            input_field: ".image-upload", // Default: .image-upload
            preview_box: ".image-preview", // Default: .image-preview
            label_field: ".image-label", // Default: .image-label
            label_default: "Chọn hình", // Default: Choose File
            label_selected: "", // Default: Change File
            no_label: true
        });
        initImage();
        tinymce.init({
            selector: 'textarea.tinymce',
            plugins: 'codesample'
        });
        $(function () {
            $('.multiple-select').multipleSelect({
                width: '100%'
            });
        });
        $(document).ready(function () {
            //init multile selects
            $.each($('select.multiple-select'), function (i, e) {
                var params = {
                    type: 'POST',
                    url: '/admin/getMeta',
                    data: {
                        name: $(e).attr('name')
                    },
                    success: function (result) {
                        if (result.code == 0) {
                            var ids = result.value.split(',');
                            $(e).multipleSelect('setSelects', ids);
                        }
                    }
                }
                $.ajax(params);
            })
        })
        $('#product-update').on('click', function() {
            try {
                var name = $('input[name=name]').val();
                var image = "";
                if (typeof($('input[name=image]').prop('files')[0]) === 'undefined') {
                    var image_token = $('input[name=image]').parent().css('background-image').split('/');
                    var image = image_token[image_token.length-1].replace('")', '');
                } else {
                    image = $('input[name=image]').prop('files')[0].name;
                }
                var description = $('textarea[name=description]').val();
                var content = tinymce.get('product_content').getContent();
                var status = $('input[name=status]:checked').val();
                var price = $('input[name=product_price]').val();
                var price_compare = $('input[name=product_price_compare]').val();
                var stock_manage = $('input[name="stock_manage"]').val();
                var collection_id = $('select[name="special-collection"]').val()[0];
                var id = $('input[name=id]').val();
            } catch (error) {
                $('.action_result span').text('Hãy nhập đầy đủ thông tin');
                window.scrollTo(document.body.scrollWidth, 0);
                return;
            }
            if (price == "" || price_compare == "") {
                $('.action_result span').text('Hãy nhập giá của sản phẩm');
                window.scrollTo(document.body.scrollWidth, 0);
                return;
            }
            if (name == "" || description == "" || content == "" || status == "") {
                $('.action_result span').text('Hãy nhập đầy đủ thông tin');
                window.scrollTo(document.body.scrollWidth, 0);
                return;
            }
            var file = $('input[name=image]').prop('files')[0];
            if (typeof(file) !== 'undefined') {
                var formdata = new FormData();
                formdata.append('image', file);
                var params1 = {
                    url: '/admin/uploadImage',
                    type: 'POST',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function(result1) {
                        var image = $.parseJSON(result1).image_name;
                        var params = {
                            url: '/admin/updateProduct',
                            type: 'POST',
                            data: {
                                name: name,
                                image: image,
                                description: description,
                                content: content,
                                status: status,
                                price: price,
                                price_compare: price_compare,
                                stock_manage: stock_manage,
                                collection_id: collection_id,
                                id: id
                            },
                            success: function(result) {
                                alert('Lưu thành công');
                                window.location.href = '/admin/product_all';
                            }
                        }
                        $.ajax(params);
                    }
                }
                $.ajax(params1);
            } else {
                var params = {
                    url: '/admin/updateProduct',
                    type: 'POST',
                    data: {
                        name: name,
                        image: image,
                        description: description,
                        content: content,
                        status: status,
                        price: price,
                        price_compare: price_compare,
                        stock_manage: stock_manage,
                        collection_id: collection_id,
                        id: id
                    },
                    success: function(result) {
                        alert('Lưu thành công');
                        window.location.href = '/admin/product_all';
                    }
                }
                $.ajax(params);
            }
        })
    })
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>