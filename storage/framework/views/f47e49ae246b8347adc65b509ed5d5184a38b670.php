<?php $__env->startSection('content'); ?>
<div class="breadcrumb-wrapper">
    <div class="breadcrumb-title">
        <h1 class="page-title"><span>Giới thiệu</span></h1>
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="http://9736.chilishop.net/index.php?route=common/home">Trang chủ</a></li>
                <li class="active"><a href="http://9736.chilishop.net/gioi-thieu.html">Giới thiệu</a></li>
            </ul>
        </div>

    </div>
</div>
<div class="container">
    <div class="row">
        <div id="content" class="col-sm-12">
            <div class="position-display">
            </div>
            <p class="MsoNormal"
               style="margin-top: 10px; margin-bottom: 15px; padding: 0px; font-family: Arial, Helvetica, sans-serif; text-align: justify;">
                <span style="font-family: arial, helvetica, sans-serif;"><span style="font-size:xx-large;">C</span>ông
                    ty CHILDREN TOYS là doanh nghiệp hàng đầu tại Việt Nam chuyên sản xuất và phân phối các loại&nbsp;đồ
                    chơi trẻ em:&nbsp;đồ chơi gỗ, đồ chơi giáo dục, đồ chơi thông minh, đồ chơi phát triển trí tuệ cho
                    trẻ em. Ngoài dòng sản phẩm mang thương hiệu Children Toys, hiện nay chúng tôi đang là Nhà phân phối
                    độc quyền Thương hiệu&nbsp;đồ chơi Ralph Lauren danh tiếng trên thế giới từ năm 1973.</span></p>
            <p class="MsoNormal"
               style="margin-top: 10px; margin-bottom: 15px; padding: 0px; font-family: Arial, Helvetica, sans-serif; text-align: justify;">
                <span style="font-family: arial, helvetica, sans-serif;">Tất cả các sản phẩm của&nbsp;</span><span
                        style="font-family: arial, helvetica, sans-serif;">CHILDREN TOYS</span><span
                        style="font-family: arial, helvetica, sans-serif;">&nbsp;sản xuất và phân phối đều đáp ứng tiêu
                    chí:</span></p>
            <ul style="margin-right: 0px; margin-bottom: 15px; margin-left: 40px; padding: 0px;  font-family: Arial, Helvetica, sans-serif;">
                <li style="margin: 0px; padding: 0px;">
                    <div style="margin: 0px; padding: 0px; text-align: justify;"><span
                                style="font-family: arial, helvetica, sans-serif;">Chất liệu tuyệt đối an toàn theo các
                            tiêu chuẩn quốc tế:&nbsp;<br>*&nbsp;Tiêu chuẩn&nbsp;ISO 9001: 2000 về hệ thống quản lý chất
                            lượng<br>* Tiêu chuẩn ASTM của Mỹ<br>*&nbsp;Tiêu chuẩn&nbsp;EN - 71 của Châu Âu<br>*&nbsp;Tiêu
                            chuẩn&nbsp;ST của Mỹ<br>* Quy chuẩn QCVN/BKHCN-2009 của Việt Nam<br></span></div>
                </li>
            </ul>
            <ul style="margin-right: 0px; margin-bottom: 15px; margin-left: 40px; padding: 0px;  font-family: Arial, Helvetica, sans-serif;">
                <li style="margin: 0px; padding: 0px;">
                    <div style="margin: 0px; padding: 0px; text-align: justify;"><span
                                style="line-height: 18px; font-family: arial, helvetica, sans-serif;">Sản phẩm mang tính
                            trí tuệ và giáo dục cao cho trẻ: đánh thức tư duy sáng tạo, tư duy logic, kích thích trí
                            tưởng tượng; rèn luyện khả năng vận động, phát triển kỹ năng làm việc theo nhóm và kỹ năng
                            hòa nhập cộng đồng;</span></div>
                </li>
                <li style="margin: 0px; padding: 0px;"><span
                            style="line-height: 18px; font-family: arial, helvetica, sans-serif;">Chủng loại sản phẩm đa
                        dạng với&nbsp;trên 700&nbsp;dòng sản phẩm;</span></li>
                <li style="margin: 0px; padding: 0px;">
                    <div style="margin: 0px; padding: 0px; text-align: justify;"><span
                                style="font-family: arial, helvetica, sans-serif;">Dịch vụ khách hàng tốt nhất, lấy
                            KHÁCH HÀNG là trung tâm của mọi sự phục vụ.</span></div>
                </li>
            </ul>
            <p style="margin-top: 10px; margin-bottom: 15px; padding: 0px;  font-family: Arial, Helvetica, sans-serif;">
                <span style="line-height: 18px; font-family: arial, helvetica, sans-serif;">Bên cạnh hoạt động sản xuất
                    kinh doanh,&nbsp;<span style="text-align: justify;">CHILDREN TOYS</span>&nbsp;đang tham gia tích cực
                    vào các hoạt động cộng đồng phục vụ trẻ em. Với mục tiêu "Tất cả vì sự phát triển toàn diện trí tuệ
                    trẻ em Việt",&nbsp;<span style="text-align: justify;">CHILDREN TOYS</span>&nbsp;đang đi những bước
                    bài bản và vững chắc đem lại một nét mới trong lĩnh vực kinh doanh dòng sản phẩm đồ chơi trí tuệ tại
                    Việt Nam.</span></p>
            <p style="margin-top: 10px; margin-bottom: 15px; padding: 0px; font-family: Arial, Helvetica, sans-serif;">
                <strong>CÔNG TY&nbsp;</strong><span
                        style="font-family: arial, helvetica, sans-serif; text-align: justify; font-weight: bold;">CHILDREN
                    TOYS</span></p>
            <p style="margin-top: 10px; margin-bottom: 15px; padding: 0px;  font-family: Arial, Helvetica, sans-serif;">
                <span style="font-size: small; font-family: arial, helvetica, sans-serif;"><strong>XƯỞNG SX ĐỒ CHƠI
                        VEETOYS</strong></span></p>
            <p style="margin-top: 10px; margin-bottom: 15px; padding: 0px;  font-family: Arial, Helvetica, sans-serif;">
                <span style="line-height: 18px; font-family: arial, helvetica, sans-serif;"></span></p>
            <p style="margin-top: 10px; margin-bottom: 15px; padding: 0px;  font-family: Arial, Helvetica, sans-serif;">
                <span style="font-size: small; font-family: arial, helvetica, sans-serif;">Số 123, Đường ABC, Quận ABC,
                    Thành Phố Hồ Chí Minh.</span></p>
            <div class="position-display">
            </div>
        </div>
    </div>
</div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>