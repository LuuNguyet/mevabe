<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
  public function getProduct() {
    $id = $_GET['id'];
    $product = \App\Models\Product::where('status', 'active')->where('id', $id)->get()->first();
    if (empty($product)) {
      return view('content.404');
    }
    \App\Models\Product::updateView($id);
    $product->collection = \App\Models\Collection::where('status', 'active')->where('id', $product->collection_id)->get()->first();
    $product->related_products = \App\Models\Product::where('status', 'active')->where('collection_id', $product->collection_id)->get();
    $product->images = \App\Models\Image::where('type', 'product')->where('type_id', $product->id)->get(['name']);    
    return view('content.product',  ['product' => $product]);
  }
}
