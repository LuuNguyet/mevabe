<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CollectionController extends Controller
{
  public function getCollection() {
    $id = $_GET['id'];
    $collection = \App\Models\Collection::where('status', 'active')->where('id', $id)->get()->first();
    if (empty($collection)) {
      return view('content.404');
    }
    \App\Models\Collection::updateView($id);
    $collection->products = \App\Models\Product::where('status', 'active')->where('collection_id', $id)->get();
    return view('content.collection', ['collection' => $collection]);
  }
}
