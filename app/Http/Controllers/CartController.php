<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CartController extends Controller
{
  public static function add(Request $request, Response $response) {
    $id = $request->input('id');
    $quantity = $request->input('quantity');
    if (!$request->session()->has('cart')) {
      $cart = (object)[];
      $cart->items = collect();
      $request->session()->put('cart', $cart);
    }
    $cart = $request->session()->get('cart');
    $product = \App\Models\Product::where('status', 'active')->where('id', $id)->get()->first();
    if (!empty($product)) {
      $quantity_cur = $cart->items->get($id) ? $cart->items->get($id) : 0;
      $cart->items->put((string)$id, $quantity + $quantity_cur);
    }
    $request->session()->put('cart', $cart);
    return response(['code'=>0, 'status'=>'success'], 200)
                  ->header('Content-Type', 'text/plain');
  }


  public static function remove(Request $request, Response $response) {
    $id = $request->input('id');
    $cart = $request->session()->get('cart');
    $cart->items->forget($id);
    $request->session()->put('cart', $cart);
    if ($cart->items->count() == 0) {
      $request->session()->forget('cart');
      return response(['code'=>-1, 'status'=>'redirect', 200])->header('Content-Type', 'text/plain');
    }
    return response(['code'=>0, 'status'=>'success', 200])->header('Content-Type', 'text/plain');
  }

  public static function change(Request $request, Response $response) {
    $id = $request->input('id');
    $quantity = $request->input('quantity');
    $cart = $request->session()->get('cart');
    $cart->items->put((string)$id, $quantity);
    $request->session()->put('cart', $cart);
    $cart_get = CartController::getCartFromSession($request);
    $item = (object)[];
    foreach ($cart_get->items as $cart_item) {
      if ($cart_item->id == $id) {
        $item = $cart_item;
      }
    }
    return response(['code'=>0, 'status'=>'success', 'data'=>$item, 200])->header('Content-Type', 'text/plain');
  }

  public static function cart(Request $request) {
    if (!$request->session()->has('cart') || count($request->session()->get('cart')->items) == 0) {
      return view('content.cart', ['message'=>'Giỏ hàng của bạn trống!']);
    }
    $cart = CartController::getCartFromSession($request);
    return view('content.cart', ['cart'=>$cart]);
  }

  public static function getCartFromSession(Request $request) {
    if (!$request->session()->has('cart')) {
      $cart = (object)[];
      $cart->items = collect();
      $request->session()->put('cart', $cart);
    }
    $cart_get = $request->session()->get('cart');
    $ids = $cart_get->items->keys()->all();
    $cart = (object)[];
    $cart->items = collect();
    $cart->total = 0;
    foreach ($ids as $id) {
      $product = \App\Models\Product::where('status', 'active')->where('id', $id)->get()->first();
      $product->quantity = $cart_get->items->get($id);
      $cart->items->push($product);
      $cart->total += $product->price * $product->quantity;
    }
    return $cart;
  }

  public static function getCart(Request $request, Response $response) {
      return response(['code'=>0, 'status'=>'success', 'data'=>CartController::getCartFromSession($request)])->header('Content-Type', 'text/plain');
  }

}
