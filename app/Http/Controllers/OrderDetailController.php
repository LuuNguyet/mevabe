<?php
/**
 * Created by PhpStorm.
 * User: Nguyet
 * Date: 9/11/2018
 * Time: 4:56 PM
 */


namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use DB;

class OrderDetailController extends Controller
{

    public function getListOrder(Request $request)
    {
        $array_order_id = array(); // array cua Nguyet, hjhj
        $data = $request->session()->get('customer')->toArray();
        $login_id = $data["id"];
        $order_id = DB::select('SELECT id FROM db_mevabe.order WHERE customer_id = ' . $login_id);
        foreach ($order_id as $key => $val) {
            array_push($array_order_id, $val->id);
        }

        $count_order_id = count($array_order_id);
        $str = '';
        if ($count_order_id < 1) {
            $str .= '1=0';
        } else {
            for ($i = 0; $i < $count_order_id; $i++) {
                if ($i == 0) {
                    $str .= 'order_id = ' . $array_order_id[$i];
                } else {
                    $str .= ' OR ';
                    $str .= 'order_id = ' . $array_order_id[$i];
                }
            }
        }
        $order_detail = DB::select('SELECT * FROM order_detail WHERE ' . $str);
        return view('content.listorder', ['order_detail' => $order_detail]);
    }


    public function getOrderDetail(Request $request)
    {
        $SHIPPING_FEE = 20000;
        $request->session()->put('shipping_fee', $SHIPPING_FEE);

        // Get order_id_created_at from session url
        $array_get_param = array(); // array get param(s) from url
        $array_data = array(); // array store key and value param(s) from url
        $data_url = $request->session()->get('_previous');
        $array_get_param = explode("?",$data_url["url"]);
        $array_data = explode("=",$array_get_param[1]); //$array_get_param[1] = 'order_id=27'
        $order_id = $array_data[1];
        $order_id_created_at =  DB::select('SELECT order_id, created_at FROM order_detail WHERE order_id = ' . $order_id);

        // Get data_cus from session customer
        $array_cus = $request->session()->get('customer')->toArray();
        $cus_id = $array_cus["id"];
        $data_cus = DB::select('SELECT * FROM customer WHERE id = ' . $cus_id);

        // Get info receiver from order_id
        $info_receiver =  DB::select('SELECT * FROM db_mevabe.order WHERE id = ' . $order_id);

        // Get info product from order_id
        $info_product =  DB::select('SELECT * 
                                        FROM products p, order_detail o 
                                        WHERE p.id = o.product_id AND o.order_id = ' . $order_id);
        $sum = 0; // Thanh tien
        foreach ($info_product as $product) {
            $sum += $product->price * $product->quantity;
        }
        $request->session()->put('sum', $sum);

        $total = $sum + $SHIPPING_FEE;
        $request->session()->put('total', $total);

        return view('content.orderdetail')
            ->with('order_id_created_at',$order_id_created_at)
            ->with('data_cus',$data_cus)
            ->with('info_receiver',$info_receiver)
            ->with('info_product',$info_product)
            ;
    }
}