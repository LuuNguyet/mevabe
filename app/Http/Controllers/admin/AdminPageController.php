<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Page;

class AdminPageController extends \App\Http\Controllers\Controller
{
  public static function getPages(Request $request) {
    if ($request->session()->has('admin')) {
      $page_name = "Tất cả";
      $page_query = Page::where('status', '!=', 'deleted')->orderBy('created_at', 'desc');
      if (strpos($request->sort, 'only') !== false) {
        $page_query = Page::where('status', explode('-', $request->sort)[1])->orderBy('created_at', 'desc');
        $page_name = explode('-', $request->sort)[1];
      }
      $page = $request->page ? $request->page : 1;
      $perpage = $request->perpage ? $request->perpage : 8;
      $total_page = $page_query->count() % $perpage > 0 ? intval($page_query->count() / $perpage) + 1 : intval($page_query->count() / $perpage);
      $total_item = $page_query->count();
      $skip = ($page - 1) * $perpage;
      $pages = $page_query->skip($skip)->take($perpage)->get();
      $current_item = count($pages);
      return view('admin.page', [
        'current_page'=>'admin/page_all',
        'pages'=>$pages,
        'page'=>$page,
        'perpage'=>$perpage,
        'total_page'=>$total_page,
        'current_items'=>$current_item,
        'total_item'=>$total_item,
        'page_name'=>$page_name
      ]);
    }else {
      return view('admin.login');
    }
  }

  public static function getPage(Request $request) {
    if ($request->session()->has('admin')) {
      $page = Page::where('id', $request->id)->get()->first();
      return view('admin.page_detail', ['current_page'=>'admin/page_all', 'page'=>$page]);
    } else {
      return view('admin.login');
    }
  }

  public static function getCreatePage(Request $request) {
    if ($request->session()->has('admin')) {
      return view('admin.page_create', ['current_page'=>'admin/page_all']);
    } else {
      return view('admin.login');
    }
  }

  public static function deletePage(Request $request) {
    if ($request->session()->has('admin')) {
      $page = Page::where('id', $request->id)->get()->first();
      $page->status = 'deleted';
      $page->save();
      return response(['code'=>0, 'status'=>'success'], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public static function createPage(Request $request) {
    if ($request->session()->has('admin')) {
      $data = (object)[];
      $data->title = $request->name;
      $data->content = $request->content;
      $data->description = $request->description;
      $data->status = $request->status;
      $page_id = Page::insert($data);
    }
    else {
      return view('admin.login');
    }
  }

  public static function updatePage(Request $request) {
    if ($request->session()->has('admin')) {
      $data = (object)[];
      $data->title = $request->name;
      $data->content = $request->content;
      $data->description = $request->description;
      $data->status = $request->status;
      $data->id = $request->id;
      $page_id = Page::update_page($data);
      return response(['code'=>0, 'status'=>'success', 'result'=>$data], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public static function getPageById(Request $request)
  {
    if ($request->session()->has('admin')) {
      $page = Page::where('id', $request->id)->get()->first();
      return response(['code' => 0, 'status' => 'success', 'data' => $page], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code' => -1, 'status' => 'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public static function quickUpdatePage(Request $request) {
    if ($request->session()->has('admin')) {
      $data = (object)[];
      $data->id = $request->id;
      $data->title = $request->title;
      $data->status = $request->status;
      Page::quickUpdate($data);
      return response(['code'=>0, 'status'=>'success'], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public static function changeStatusPage(Request $request) {
    if ($request->session()->has('admin')) {
      $ids = $request->ids;
      $status = $request->status;
      foreach ($ids as $id) {
        Page::changeStatus($id, $status);
      }
      return response(['code'=>0, 'status'=>'success'], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code' => -1, 'status' => 'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

}