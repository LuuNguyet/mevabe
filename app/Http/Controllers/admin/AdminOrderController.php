<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Order;
use App\Models\Customer;
use App\Models\OrderDetail;
use App\Models\Product;
class AdminOrderController extends \App\Http\Controllers\Controller
{
  public static function getOrders(Request $request) {
    if ($request->session()->has('admin')) {
      $order_query = Order::orderBy('created_at', 'desc');
      $page_name = "Tất cả";
      if (strpos($request->sort, 'only') !== false) {
        $order_query = Order::where('order_status', explode('-', $request->sort)[1])->orderBy('created_at', 'desc');
        $page_name = explode('-', $request->sort)[1];
      }
      $page = $request->page ? $request->page : 1;
      $perpage = $request->perpage ? $request->perpage : 32;
      $total_page = $order_query->count() % $perpage > 0 ? intval($order_query->count() / $perpage) + 1 : intval($order_query->count() / $perpage);
      $total_item = $order_query->count();
      $skip = ($page - 1) * $perpage;
      $orders = $order_query->skip($skip)->take($perpage)->get();
      foreach ($orders as $order) {
        $order->customer = Customer::where('id', $order->customer_id)->get()->first();
      }
      $current_item = count($orders);
      return view('admin.order', [
        'current_page'=>'admin/order',
        'orders'=>$orders,
        'page'=>$page,
        'perpage'=>$perpage,
        'total_page'=>$total_page,
        'current_items'=>$current_item,
        'total_item'=>$total_item,
        'page_name'=>$page_name
       ]);

      $orders = Order::orderBy('created_at', 'desc')->get();
      return view('admin.order', ['current_page'=>'admin/order', 'orders'=>$orders]);
    } else {
      return view('admin.login');
    }
  }

  public static function getOrder(Request $request) {
    if ($request->session()->has('admin')) {
      $order = Order::where('id', $request->id)->get()->first();
      if ($order) {
        $order->customer = Customer::where('id', $order->customer_id)->get()->first();
        $order->items = OrderDetail::where('order_id', $order->id)->get();
        foreach ($order->items as $item) {
          $item->product = Product::where('id', $item->product_id)->get()->first();
        }
        return view('admin.order_detail', ['current_page'=>'admin/order', 'order'=>$order]);
      } else {
        return redirect('/admin/order_all');
      }
    } else {
      return view('admin.login');
    }
  }
  public static function changeStatusOrder(Request $request) {
    if ($request->session()->has('admin')) {
      $ids = $request->ids;
      $status = $request->status;
      foreach ($ids as $id) {
        Order::changeStatus($id, $status);
      }
      return response(['code'=>0, 'status'=>'success'], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public static function getOrderById(Request $request) {
    if ($request->session()->has('admin')) {
      $order = Order::where('id', $request->id)->get()->first();
      return response(['code'=>0, 'status'=>'success', 'data'=>$order], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public static function quickUpdateOrder(Request $request) {
    if ($request->session()->has('admin')) {
      $data = (object)[];
      $data->id = $request->id;
      $data->total = $request->total;
      $data->order_status = $request->order_status;
      Order::quickUpdate($data);
      return response(['code'=>0, 'status'=>'success'], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }
}
