<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use App\Models\Collection;
use App\Models\Product;
use App\Models\Meta;

class SettingsController extends \App\Http\Controllers\Controller
{
  public static function getSettings(Request $request) {
    if ($request->session()->has('admin')) {
      $metas = \App\Models\Meta::get();
      $settings = collect();
      foreach ($metas as $meta) {
        $settings->put($meta->name, $meta->value);
      }
      return view('admin.settings', ['current_page'=>'admin/settings', 'settings'=>$settings]);
    } else {
      return view('admin.login');
    }
  }
  public static function insert(Request $request, Response $response) {
    $data = (object)[];
    $data->name = $request->input('name');
    $data->value = $request->input('value') ? $request->input('value') : "";
    \App\Models\Meta::insert($data);
    return response(['code'=>0,'result'=>'success', 'data'=>$data], 200)->header('Content-Type', 'text/plain');
  }

  public static function getMeta(Request $request, Response $response) {
    //Return meta with specyfic name;
    $name = $request->input('name');
    $meta = \App\Models\Meta::where('name', $name)->get()->first();
    if (empty($meta)) {
      $value = "";
    } else {
      $value = $meta->value;
    }
    return response(['code'=>0,'value'=>$value], 200);
  }

  public static function uploadImage(Request $request) {
      if (Input::hasFile('image')) {
        $image = Input::file('image');
        $image_name = time() . $image->getClientOriginalName();
        $image->move('uploads', $image_name);
        return response(['code'=>0, 'status'=>'success', 'image_name'=>$image_name], 200)->header('Content-Type', 'text/plain');
      } else {
        return response(['code'=>0, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
      }
  }

  public static function getImage(Request $request) {
      $value = "";
      if (isset($request->type)) {
        switch ($request->type) {
          case 'collection':
            $value = Collection::where('id', $request->key)->get()->first()->image;
            break;
          case 'product':
            $value = Product::where('id', $request->key)->get()->first()->featured_image;
            break;
        }
      } else {
        $value = Meta::where('name', $request->key)->get()->first()->value;
      }
      return response(['code'=>0,'status'=>'success','data'=>$value], 200)->header('Content-Type', 'text/plain');
  }
}
