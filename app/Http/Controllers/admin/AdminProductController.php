<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Product;

class AdminProductController extends \App\Http\Controllers\Controller
{
  public static function getProducts(Request $request)
  {
    if ($request->session()->has('admin')) {
      $page_name = "Tất cả";
      $product_query = Product::where('status', '!=', 'deleted')->orderBy('created_at', 'desc');
      if (strpos($request->sort, 'only') !== false) {
        $product_query = Product::where('status', explode('-', $request->sort)[1])->orderBy('created_at', 'desc');
        $page_name = explode('-', $request->sort)[1];
      }
      $page = $request->page ? $request->page : 1;
      $perpage = $request->perpage ? $request->perpage : 8;
      $total_page = $product_query->count() % $perpage > 0 ? intval($product_query->count() / $perpage) + 1 : intval($product_query->count() / $perpage);
      $total_item = $product_query->count();
      $skip = ($page - 1) * $perpage;
      $products = $product_query->skip($skip)->take($perpage)->get();
      $current_item = count($products);
      return view('admin.product', [
        'current_page' => 'admin/product_all',
        'products' => $products,
        'page' => $page,
        'perpage' => $perpage,
        'total_page' => $total_page,
        'current_items' => $current_item,
        'total_item' => $total_item,
        'page_name' => $page_name
      ]);
    } else {
      return view('admin.login');
    }
  }

  public static function getCreateProduct(Request $request)
  {
    if ($request->session()->has('admin')) {
      return view('admin.product_create', ['current_page' => 'admin/product_all']);
    } else {
      return view('admin.login');
    }
  }

  public static function createProduct(Request $request)
  {
    if ($request->session()->has('admin')) {
      $data = (object)[];
      $data->title = $request->name;
      $data->featured_image = $request->image;
      $data->description = $request->description;
      $data->content = $request->content;
      $data->status = $request->status;
      $data->price = $request->price;
      $data->price_compare = $request->price_compare;
      $data->stock_manage = $request->stock_manage;
      $data->collection_id = $request->collection_id;

      $product_id = Product::insert($data);
      return response(['code' => 0, 'status' => 'success', 'result' => $data], 200)->header('Content-Type', 'text/plain');
    } else {
      return view('admin.login');
    }
  }

  public static function deleteProduct(Request $request)
  {
    if ($request->session()->has('admin')) {
      $product = product::where('id', $request->id)->get()->first();
      $product->status = 'deleted';
      $product->save();
      return response(['code' => 0, 'status' => 'success'], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code' => -1, 'status' => 'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public static function getProduct(Request $request)
  {
    if ($request->session()->has('admin')) {
      $product = Product::where('id', $request->id)->get()->first();
      return view('admin.product_detail', ['current_page'=>'admin/product_all', 'product'=>$product]);
    } else {
      return view('admin.login');
    }
  }

  public static function updateProduct(Request $request) {
    if ($request->session()->has('admin')) {
      $data = (object)[];
      $data->title = $request->name;
      $data->image = $request->image;
      $data->description = $request->description;
      $data->content = $request->content;
      $data->status = $request->status;
      $data->id = $request->id;
      $data->price = $request->price;
      $data->price_compare = $request->price_compare;
      $data->stock_manage = $request->stock_manage;
      $data->collection_id = $request->collection_id;

      $product_id = Product::update_product($data);
      return response(['code'=>0, 'status'=>'success', 'result'=>$data], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public static function changeStatusProduct(Request $request) {
    if ($request->session()->has('admin')) {
      $ids = $request->ids;
      $status = $request->status;
      foreach ($ids as $id) {
        Product::changeStatus($id, $status);
      }
      return response(['code'=>0, 'status'=>'success'], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public static function getProductById(Request $request)
  {
    if ($request->session()->has('admin')) {
      $product = Product::where('id', $request->id)->get()->first();
      return response(['code' => 0, 'status' => 'success', 'data' => $product], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code' => -1, 'status' => 'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }
}
