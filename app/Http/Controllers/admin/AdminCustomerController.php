<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Customer;

class AdminCustomerController extends \App\Http\Controllers\Controller
{
  public function updateInfo(Request $request, Response $response)
  {
    if ($request->session()->has('admin')) {
      $data = (object)[];
      $data->name = $request->name;
      $data->email = $request->email;
      $data->phone = $request->phone;
      $data->address = $request->address;

      $data->id = $request->id;
      $customer_id = Customer::updateInfo($data);
      return response(['code'=>0, 'status'=>'success', 'result'=>$data], 200)->header('Content-Type', 'text/plain');
  } else {
      return response(['code' => -1, 'status' => 'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }
}