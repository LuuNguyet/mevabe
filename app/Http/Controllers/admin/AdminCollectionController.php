<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Collection;

class AdminCollectionController extends \App\Http\Controllers\Controller
{
  public static function getCollections(Request $request) {
    if ($request->session()->has('admin')) {
      $page_name = "Tất cả";
      $collection_query = Collection::where('status', '!=', 'deleted')->orderBy('created_at', 'desc');
      if (strpos($request->sort, 'only') !== false) {
        $collection_query = Collection::where('status', explode('-', $request->sort)[1])->orderBy('created_at', 'desc');
        $page_name = explode('-', $request->sort)[1];
      }
      $page = $request->page ? $request->page : 1;
      $perpage = $request->perpage ? $request->perpage : 8;
      $total_page = $collection_query->count() % $perpage > 0 ? intval($collection_query->count() / $perpage) + 1 : intval($collection_query->count() / $perpage);
      $total_item = $collection_query->count();
      $skip = ($page - 1) * $perpage;
      $collections = $collection_query->skip($skip)->take($perpage)->get();
      $current_item = count($collections);
      return view('admin.collection', [
        'current_page'=>'admin/collection_all',
        'collections'=>$collections,
        'page'=>$page,
        'perpage'=>$perpage,
        'total_page'=>$total_page,
        'current_items'=>$current_item,
        'total_item'=>$total_item,
        'page_name'=>$page_name
       ]);
    } else {
      return view('admin.login');
    }
  }
  public static function getCollection(Request $request) {
    if ($request->session()->has('admin')) {
      $collection = Collection::where('id', $request->id)->get()->first();
      return view('admin.collection_detail', ['current_page'=>'admin/collection_all', 'collection'=>$collection]);
    } else {
      return view('admin.login');
    }
  }

  public static function getCreateCollection(Request $request) {
    if ($request->session()->has('admin')) {
      return view('admin.collection_create', ['current_page'=>'admin/collection_all']);
    } else {
      return view('admin.login');
    }
  }

  public static function createCollection(Request $request) {
    if ($request->session()->has('admin')) {
      $data = (object)[];
      $data->title = $request->name;
      $data->image = $request->image;
      $data->content = $request->content;
      $data->description = $request->description;
      $data->status = $request->status;
      $collection_id = Collection::insert($data);
      return response(['code'=>0, 'status'=>'success', 'result'=>$data], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public static function updateCollection(Request $request) {
    if ($request->session()->has('admin')) {
      $data = (object)[];
      $data->title = $request->name;
      $data->image = $request->image;
      $data->content = $request->content;
      $data->description = $request->description;
      $data->status = $request->status;
      $data->id = $request->id;
      $collection_id = Collection::update_collection($data);
      return response(['code'=>0, 'status'=>'success', 'result'=>$data], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public static function deleteCollection(Request $request) {
    if ($request->session()->has('admin')) {
      $collection = Collection::where('id', $request->id)->get()->first();
      $collection->status = 'deleted';
      $collection->save();
      return response(['code'=>0, 'status'=>'success'], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public static function getCollectionById(Request $request) {
    if ($request->session()->has('admin')) {
      $collection = Collection::where('id', $request->id)->get()->first();
      return response(['code'=>0, 'status'=>'success', 'data'=>$collection], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public static function quickUpdateCollection(Request $request) {
    if ($request->session()->has('admin')) {
      $data = (object)[];
      $data->id = $request->id;
      $data->title = $request->title;
      $data->image = $request->image;
      $data->status = $request->status;
      Collection::quickUpdate($data);
      return response(['code'=>0, 'status'=>'success'], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public static function changeStatusCollection(Request $request) {
    if ($request->session()->has('admin')) {
      $ids = $request->ids;
      $status = $request->status;
      foreach ($ids as $id) {
        Collection::changeStatus($id, $status);
      }
      return response(['code'=>0, 'status'=>'success'], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }
}
