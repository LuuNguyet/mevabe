<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AdminDashboardController extends \App\Http\Controllers\Controller
{
  public static function getDashboard(Request $request) {
    if ($request->session()->has('admin')) {
      return view('admin.dashboard', ['current_page'=>'admin']);
    } else {
      return view('admin.login');
    }
  }

  public static function login(Request $request, Response $response) {
      try {
        $email = $request->input('email');
        $password = $request->input('password');
        $admin = \App\Models\Admin\User::where('email', $email)->where('password', $password)->get()->first();
        if (!empty($admin)) {
          $sidebar_list = collect([]);
          $sidebar_list->push((object)['href'=>'admin', 'title'=>'Bảng điều khiển', 'icon'=>'fa-home']);
          $sidebar_list->push((object)['href'=>'admin/order', 'title'=>'Đơn hàng', 'icon'=>'fa-cart-plus']);
          $sidebar_list->push((object)['href'=>'admin/collection_all', 'title'=>'Nhóm sản phẩm', 'icon'=>'fa-book']);
          $sidebar_list->push((object)['href'=>'admin/product_all', 'title'=>'Sản phẩm', 'icon'=>'fa-product-hunt']);
          $sidebar_list->push((object)['href'=>'admin/page_all', 'title'=>'Trang nội dung', 'icon'=>'fa-pencil-square-o']);
          $sidebar_list->push((object)['href'=>'admin/settings', 'title'=>'Thiết lập chung', 'icon'=>'fa-cog']);
          $request->session()->put('sidebar_list', $sidebar_list);
          $request->session()->put('admin', $admin);
          return response(['code'=> 0, 'status'=>'success'], 200)->header('Content-Type', 'text/plain');
        } else {
          return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
        }
      } catch (\Throwable $e) {
        error_log('Message ' . $e->getMessage());
      }
  }
//  private static function sidebarList() {
//    $sidebar_list = collect([]);
//    $sidebar_list->push((object)['href'=>'admin', 'title'=>'Bảng điều khiển', 'icon'=>'fa-home']);
//    $sidebar_list->push((object)['href'=>'admin/order', 'title'=>'Đơn hàng', 'icon'=>'fa-cart-plus']);
//    $sidebar_list->push((object)['href'=>'admin/collection_all', 'title'=>'Nhóm sản phẩm', 'icon'=>'fa-book']);
//    $sidebar_list->push((object)['href'=>'admin/product_all', 'title'=>'Sản phẩm', 'icon'=>'fa-product-hunt']);
//    $sidebar_list->push((object)['href'=>'admin/page_all', 'title'=>'Trang nội dung', 'icon'=>'fa-pencil-square-o']);
//
//
//    $sidebar_list->push((object)['href'=>'admin/settings', 'title'=>'Thiết lập chung', 'icon'=>'fa-cog']);
//    return $sidebar_list;
//  }

  public static function logout(Request $request) {
    $request->session()->forget('admin');
    $request->session()->forget('sidebar_list');
    return redirect('admin');
  }
}
