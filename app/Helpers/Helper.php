<?php

namespace App\Helpers;

use App\Models\Meta;
use App\Models\Order;
use Illuminate\Support\Facades\Session;
//{!! Helper::shout('this is how to use autoloading correctly!!') !!}
class Helper
{
  public static function getMeta($name)
  {
    //Return meta with specyfic name;
    $meta = \App\Models\Meta::where('name', $name)->get()->first();
    if (empty($meta)) {
      $value = "";
    } else {
      $value = $meta->value;
    }
    return $value;
  }

  public static function getCollection($id) {
      $collection = \App\Models\Collection::where('status', 'active')->where('id', $id)->get()->first();
      if (!empty($collection)){
        $collection->products = \App\Models\Product::where('status', 'active')->where('collection_id', $id)->get();
        return $collection;
      }
      return "";
  }

  public static function getOrderByCustomerID($id) {
    return Order::where('customer_id', $id)->get();
  }

  public static function getProduct($ids) {
    $ids = explode(',', $ids);
    $products = \App\Models\Product::where('status', 'active')->whereIn('id', $ids)->get();
    return $products;
  }

  public static function getAllCollection()
  {
    //Collection -> Return all collections with counter $collection->number_of_product
    $collections = \App\Models\Collection::where('status', 'active')->orderBy('created_at', 'desc')->get();
    foreach ($collections as $collection) {
      $collection->number_of_product = \App\Models\Product::where('status', 'active')->where('collection_id', $collection->id)->get()->count();
    }
    return $collections;
  }
  public static function getAllProduct()
  {
    //Collection -> Return all collections with counter $collection->number_of_product
    $products = \App\Models\Product::where('status', 'active')->orderBy('created_at', 'desc')->get();
    return $products;
  }
  public static function getProducts($ids)
  {
    //Get product by ids - array
    $products = \App\Models\Product::wherein('id', $ids)->where('status', 'active')->get();
    return $products;
  }

  public static function getCollections($ids)
  {
    //Get product by ids - array
    $collection = \App\Models\Collection::wherein('id', $ids)->where('status', 'active')->get();
    return $collection;
  }

  public static function formatMoney($money, $format = ',')
  {
    if (!$money) return 0;
    if ($format == ',') return number_format($money);
    return number_format($money, 0, '.', '.');
  }

  public static function getCartFromSession() {
    if(!Session::has('cart'))
      return false;
    $cart_get = Session::get('cart');
    $ids = $cart_get->items->keys()->all();
    $cart = (object)[];
    $cart->items = collect();
    $cart->total = 0;
    foreach ($ids as $id) {
      $product = \App\Models\Product::where('status', 'active')->where('id', $id)->get()->first();
      $product->quantity = $cart_get->items->get($id);
      $cart->items->push($product);
      $cart->total += $product->price * $product->quantity;
    }
    return $cart;
  }

}
