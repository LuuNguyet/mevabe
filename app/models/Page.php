<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
  protected $table = 'page';

  public static function insert($data) {
    $page = new Page;
    $page->title = $data->title;
    $page->description = $data->description;
    $page->content = $data->content;
    $page->created_at = date('Y-m-d H:i:s');
    $page->updated_at = date('Y-m-d H:i:s');
    $page->status = $data->status;
    $page->view = isset($data->view) ? $data->view : 0;
    $page->save();
    return $page->id;
  }

  public static function updateView($id) {
    $page = Page::find($id);
    if ($page) {
      $view = 1;
      if ($page->view)
        $view = $page->view + 1;
      $page->view = $view;
      $page->save();
    }
  }
  
  public static function update_page($data) {
    $page = Page::find($data->id);
    $page->title = $data->title;
    $page->description = $data->description;
    $page->content = $data->content;
    $page->updated_at = date('Y-m-d H:i:s');
    $page->status = $data->status;
    $page->save();
    return $page->id;
  }

  public static function changeStatus($id, $status) {
    $page = Page::where('id', $id)->get()->first();
    $page->updated_at = date('Y-m-d H:i:s');
    $page->status = $status;
    $page->save();
    return $page->id;
  }

  public static function quickUpdate($data) {
    $page = Page::where('id', $data->id)->get()->first();
    $page->title = $data->title;
    $page->updated_at = date('Y-m-d H:i:s');
    $page->status = $data->status;
    $page->save();
    return $page->id;
  }
}