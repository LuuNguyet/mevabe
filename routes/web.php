<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Admin
// Route::get('admin', [
//   'as'=>'admin',
//   'uses'=>'AdminDashboardController@getDashboard'
// ]);
// Route::get('admin/{action}', function ($action) {
//     include 'admin.php';
// });
if (\Request::is('admin/*') || \Request::is('admin')) {
  require 'admin.php';
}
//Index
Route::get('index',[
  'as'=>'trang-chu',
  'uses'=>'IndexController@getIndex'
]);
Route::get('/',[
  'as'=>'trang-chu',
  'uses'=>'IndexController@getIndex'
]);

//Collection
Route::get('collection',[
  'as'=>'collection',
  'uses'=>'CollectionController@getCollection'
]);

//Product
Route::get('product',[
  'as'=>'product',
  'uses'=>'ProductController@getProduct'
]);


//Login
Route::get('login',[
  'as'=>'login',
  'uses'=>'CustomerController@getLogin'
]);

//Function
Route::post('login',[
  'as'=>'login',
  'uses'=>'CustomerController@login'
]);

//Logout
Route::get('logout',[
  'as'=>'logout',
  'uses'=>'CustomerController@logout'
]);

//Signin
Route::post('signin', [
  'as'=>'sigin',
  'uses'=>'CustomerController@signin'
]);

Route::get('signin', [
  'as'=>'sigin',
  'uses'=>'CustomerController@getSignin'
]);

Route::post('addToCart', [
  'as'=>'addToCart',
  'uses'=>'CartController@add'
]);
Route::post('removeItem', [
  'as'=>'removeItem',
  'uses'=>'CartController@remove'
]);
Route::post('changeItem', [
  'as'=>'changeItem',
  'uses'=>'CartController@change'
]);
Route::get('cart', [
  'as'=>'cart',
  'uses'=>'CartController@cart'
]);


Route::get('removeItem', [
  'as'=>'cart',
  'uses'=>'CartController@removeItem'
]);

Route::post('getCart', [
  'as'=>'cart',
  'uses'=>'CartController@getCart'
]);

Route::get('checkout', [
  'as'=>'checkout',
  'uses'=>'CheckoutController@getCheckout'
]);
Route::post('checkout', [
  'as'=>'checkout',
  'uses'=>'CheckoutController@checkout'
]);

Route::get('success', [
  'as'=>'checkout',
  'uses'=>'CheckoutController@getSuccess'
]);

//Page
Route::get('page', [
  'as'=>'page',
  'uses'=>'PageController@getPage'
]);

//Contact
Route::get('contact', [
  'as'=>'contact',
  'uses'=>'ContactController@getContact'
]);

Route::post('contact', [
  'as'=>'contact',
  'uses'=>'ContactController@contact'
]);

Route::get('customer', [
  'as'=>'customer',
  'uses'=>'CustomerController@getCustomer'
]);

//change info customer
Route::get('changeinfo', [
  'as'=>'customer',
  'uses'=>'CustomerController@getchangeInfo'
]);

Route::post('changeInfo', [
  'as'=>'customer',
  'uses'=>'CustomerController@changeInfo'
]);

//change passwd
Route::get('changepasswd', [
  'as'=>'customer',
  'uses'=>'CustomerController@getchangePasswd'
]);

Route::post('changePasswd', [
  'as'=>'customer',
  'uses'=>'CustomerController@changePasswd'
]);

//change address
Route::get('changeaddress', [
  'as'=>'customer',
  'uses'=>'CustomerController@getchangeAddress'
]);

// form new address
Route::get('newaddress',[
  'as'=>'customer',
  'uses'=>'CustomerController@changeaddress'
]);

//update new address
Route::post('changeAddress',[
  'as'=>'customer',
  'uses'=>'CustomerController@newaddress'
]);

//ListOrder
Route::get('listorder', [
    'as'=>'listorder',
    'uses'=>'OrderDetailController@getListOrder'
]);

Route::get('orderdetail', [
    'as'=>'orderdetail',
    'uses'=>'OrderDetailController@getOrderDetail'
]);
