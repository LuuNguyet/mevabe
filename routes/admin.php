<?php
Route::get('admin', [
  'as'=>'admin',
  'uses'=>'admin\AdminDashboardController@getDashboard'
]);

Route::post('admin/login', [
  'as'=>'admin',
  'uses'=>'admin\AdminDashboardController@login'
]);

Route::get('admin/logout', [
  'as'=>'admin',
  'uses'=>'admin\AdminDashboardController@logout'
]);

//Customer
//------------------------------>
Route::post('admin/customer/updateInfo', [
  'as'=>'customer',
  'uses'=>'admin\AdminCustomerController@updateInfo'
]);

//Settings
//------------------------------>
Route::get('admin/settings', [
  'as'=>'admin',
  'uses'=>'admin\SettingsController@getSettings'
]);


//Meta
//------------------------------>
Route::post('admin/updateMeta', [
  'as'=>'admin',
  'uses'=>'admin\SettingsController@insert'
]);
Route::post('admin/getMeta', [
  'as'=>'admin',
  'uses'=>'admin\SettingsController@getMeta'
]);
Route::post('admin/uploadImage', [
  'as'=>'admin',
  'uses'=>'admin\SettingsController@uploadImage'
]);

//Page
//------------------------------>
Route::get('/admin/page_all', [
  'as'=>'admin',
  'uses'=>'admin\AdminPageController@getPages'
]);

Route::get('admin/createPage', [
  'as'=>'admin',
  'uses'=>'admin\AdminPageController@getCreatePage'
]);

Route::post('/admin/createPage', [
  'as'=>'admin',
  'uses'=>'admin\AdminPageController@createPage'
]);

Route::post('admin/updatePage', [
  'as'=>'admin',
  'uses'=>'admin\AdminPageController@updatePage'
]);

Route::get('admin/page', [
  'as'=>'admin',
  'uses'=>'admin\AdminPageController@getPage'
]);

Route::post('/admin/deletePage', [
  'as'=>'admin',
  'uses'=>'admin\AdminPageController@deletePage'
]);

Route::post('/admin/getPageById', [
  'as'=>'admin',
  'uses'=>'admin\AdminPageController@getPageById'
]);

Route::post('/admin/changeStatusPage', [
  'as'=>'admin',
  'uses'=>'admin\AdminPageController@changeStatusPage'
]);

Route::post('/admin/quickUpdatePage', [
  'as'=>'admin',
  'uses'=>'admin\AdminPageController@quickUpdatePage'
]);

//Collection
//------------------------------>
Route::get('admin/collection_all', [
  'as'=>'admin',
  'uses'=>'admin\AdminCollectionController@getCollections'
]);

Route::get('admin/collection', [
  'as'=>'admin',
  'uses'=>'admin\AdminCollectionController@getCollection'
]);

Route::get('admin/createCollection', [
  'as'=>'admin',
  'uses'=>'admin\AdminCollectionController@getCreateCollection'
]);

Route::post('admin/createCollection', [
  'as'=>'admin',
  'uses'=>'admin\AdminCollectionController@createCollection'
]);

Route::post('admin/deleteCollection', [
  'as'=>'admin',
  'uses'=>'admin\AdminCollectionController@deleteCollection'
]);

Route::post('admin/updateCollection', [
  'as'=>'admin',
  'uses'=>'admin\AdminCollectionController@updateCollection'
]);

Route::post('admin/getCollectionById', [
  'as'=>'admin',
  'uses'=>'admin\AdminCollectionController@getCollectionById'
]);

Route::post('admin/quickUpdateCollection', [
  'as'=>'admin',
  'uses'=>'admin\AdminCollectionController@quickUpdateCollection'
]);

Route::post('admin/changeStatusCollection', [
  'as'=>'admin',
  'uses'=>'admin\AdminCollectionController@changeStatusCollection'
]);

Route::post('admin/getImage', [
  'as'=>'admin',
  'uses'=>'admin\SettingsController@getImage'
]);
//Order
//------------------------------>
Route::get('admin/order_all', [
  'as'=>'admin',
  'uses'=>'admin\AdminOrderController@getOrders'
]);

Route::get('admin/order', [
  'as'=>'admin',
  'uses'=>'admin\AdminOrderController@getOrder'
]);

Route::post('admin/changeStatusOrder', [
  'as'=>'admin',
  'uses'=>'admin\AdminOrderController@changeStatusOrder'
]);

Route::post('admin/getOrderById', [
  'as'=>'admin',
  'uses'=>'admin\AdminOrderController@getOrderById'
]);

Route::post('admin/quickUpdateOrder', [
  'as'=>'admin',
  'uses'=>'admin\AdminOrderController@quickUpdateOrder'
]);

//Product
//------------------------------>
Route::get('admin/product_all', [
  'as'=>'admin',
  'uses'=>'admin\AdminProductController@getProducts'
]);

Route::get('admin/createProduct', [
  'as'=>'admin',
  'uses'=>'admin\AdminProductController@getCreateProduct'
]);

Route::post('admin/createProduct', [
  'as'=>'admin',
  'uses'=>'admin\AdminProductController@createProduct'
]);

Route::post('admin/deleteProduct', [
  'as'=>'admin',
  'uses'=>'admin\AdminProductController@deleteProduct'
]);

Route::post('admin/updateProduct', [
  'as'=>'admin',
  'uses'=>'admin\AdminProductController@updateProduct'
]);

Route::post('admin/changeStatusProduct', [
  'as'=>'admin',
  'uses'=>'admin\AdminProductController@changeStatusProduct'
]);

Route::post('admin/deleteProduct', [
  'as'=>'admin',
  'uses'=>'admin\AdminProductController@deleteProduct'
]);

Route::get('admin/product', [
  'as'=>'admin',
  'uses'=>'admin\AdminProductController@getProduct'
]);

Route::get('admin/getProductById', [
  'as'=>'admin',
  'uses'=>'admin\AdminProductController@getProductById'
]);