@extends('master')
@section('content')
    <body class="account-order">
    <div class="breadcrumb-wrapper">
        <div class="breadcrumb-title">
            <h1 class="page-title"><span>Lịch Sử Đặt Hàng</span></h1>
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="./index">Trang chủ</a></li>
                    <li><a href="./customer">Tài khoản</a></li>
                    <li class="active"><a href="http://9736.chilishop.net/index.php?route=account/order">Lịch Sử Đặt Hàng</a></li>
                </ul>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-12">
                <div class="position-display"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-right">Mã đơn hàng</td>
                            <td class="text-left">Tình trạng</td>
                            <td class="text-left">Ngày tạo</td>
                            <td class="text-right">Số lượng Đặt mua</td>
                            <td class="text-left">Khách Hàng</td>
                            <td class="text-right">Tổng Cộng</td>
                            <td></td>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($order_detail as $order_detail)
                        <tr>
                            <td class="text-right">{{$order_detail->order_id}}</td>
                            <td class="text-left">{{$order_detail->status}}</td>
                            <td class="text-left">{{$order_detail->created_at}}</td>
                            <td class="text-right">{{$order_detail->quantity}}</td>
                            <td class="text-left">{{Session::get('customer')->email}}</td>
                            <td class="text-right">{{$order_detail->unit_price}}</td>
                            <td class="text-right"><a href="./orderdetail?order_id={{$order_detail->order_id}}" data-toggle="tooltip" title="" class="btn btn-info" data-original-title="Xem"><i class="fa fa-eye"></i></td>
                        </tr>
                         @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="text-right"></div>
                <div class="buttons clearfix button-box">
                    <div class="pull-right"><a href="http://9736.chilishop.net/index.php?route=account/account" class="btn btn-primary">Tiếp tục</a></div>
                </div>
                <div class="position-display">
                </div>
            </div>
        </div>
    </div>
    </body>
@endsection