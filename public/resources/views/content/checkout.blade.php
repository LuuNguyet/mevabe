@extends('master')
@section('content')
    <div class="breadcrumb-wrapper">
        <div class="breadcrumb-title">
            <h1 class="page-title"><span>Thanh Toán</span></h1>
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Trang chủ</a></li>
                    <li><a href="cart">Giỏ Hàng</a></li>
                    <li class="active"><a href="checkout">Thanh Toán</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-12">
                <div class="position-display">
                </div>
                <!-- Quick Checkout v4.0 by Dreamvention.com checkout/quickcheckout.tpl -->
                <!-- Quick Checkout v5.0.0 by Dreamvention.com module/d_quickcheckout.tpl -->
                <style>
                    #quickcheckout {
                        max-width: px;
                        margin: 0 auto;
                    }

                    .blocks {
                        display: none;
                    }

                    #step_1 {
                        display: block;
                    }

                    [data-toggle="tooltip"]:after {
                        font-family: FontAwesome;
                        color: #1E91CF;
                        content: "\f059";
                        margin-left: 4px;
                    }
                </style>
                <div id="quickcheckout">
                    <div class="wait" style="display: none;">
                        <span class="preloader">
                            <div class="spinner" role="progressbar"
                                 style="position: absolute; width: 0px; z-index: 2000000000; left: 50%; top: 50%;">
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-0-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(0deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-1-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(45deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-2-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(90deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-3-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(135deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-4-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(180deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-5-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(225deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-6-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(270deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-7-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(315deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                            </div>
                        </span>
                    </div>
                    <div class="processing-payment">
                        <span class="preloader">
                            <div class="spinner" role="progressbar"
                                 style="position: absolute; width: 0px; z-index: 2000000000; left: 50%; top: 50%;">
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-0-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(0deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-1-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(45deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-2-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(90deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-3-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(135deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-4-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(180deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-5-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(225deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-6-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(270deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                                <div style="position: absolute; top: -2px; opacity: 0.25; animation: opacity-100-25-7-8 1s linear 0s infinite normal none running;">
                                    <div style="position: absolute; width: 10px; height: 4px; background: rgb(102, 102, 102); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px; transform-origin: left center 0px; transform: rotate(315deg) translate(4px, 0px); border-radius: 2px;"></div>
                                </div>
                            </div>
                        </span>
                    </div>
                    <div class="wrap">
                        {{--
                        <div class="block-title">Sử dụng phiếu quà tặng </div>
                        --}}
                        <div class="block-title">Thanh Toán</div>
                        <div class="block-content">
                            <div class="aqc-column aqc-column-0">
                                <div id="step_1" data-sort="1" data-row="0" data-column="0" data-width="50"
                                     class="blocks">
                                    <!-- Quick Checkout v4.1.2 by Dreamvention.com quickcheckout/login.tpl -->
                                {{--<div id="option_login_popup_trigger_wrap" class="form-inline clearfix ">--}}
                                {{--<!-- #option_register_popup-->--}}
                                {{--<div class="btn-group">--}}
                                {{--<label id="option_register_popup" style="width:60%" class="btn btn-default ">--}}
                                {{--<input type="radio" name="account" value="register" id="register" class="hidden" data-refresh="1" autocomplete="off">--}}
                                {{--<a href="signin">Đăng ký tài khoản</a>       </label>--}}
                                {{--<!-- #option_guest_popup-->--}}
                                {{--<label id="option_guest_popup" style="width:40%" class="btn btn-primary  ">--}}
                                {{--<input type="radio" name="account" value="guest" id="guest" checked="checked" class="hidden" data-refresh="1" autocomplete="off">--}}
                                {{--<a href="checkout">Thanh toán</a>        </label>--}}
                                {{--</div>--}}
                                {{--<!-- option_login_popup_trigger -->--}}
                                {{--<a id="option_login_popup_trigger" href="login" data-toggle="modal" data-target="#option_login_popup" class="button btn btn-primary pull-right ">Đăng nhập</a>--}}
                                {{--</div>--}}
                                <!-- Modal #option_login_popup -->
                                    <script>
                                        <!--
                                        $(function () {
                                            if ($.isFunction($.fn.uniform)) {
                                                $(" .styled, input:radio.styled").uniform().removeClass('styled');
                                            }

                                            $(document).on('click', '.qc-dsl-button', function () {
                                                $('.qc-dsl-button').find('.l-side').spin(false);
                                                $(this).find('.l-side').spin('icons', '#fff');

                                                $('.qc-dsl-button').find('.qc-dsl-icon').removeClass('qc-dsl-hide-icon');
                                                $(this).find('.qc-dsl-icon').addClass('qc-dsl-hide-icon');
                                            })
                                        })
                                        //-->
                                    </script>
                                </div>
                            </div>
                            @if (!Session::has('customer'))
                                Vui lòng <a href="login">Đăng nhập</a> hoặc<a href="signin"> Đăng ký </a>trước khi đặt
                                hàng!
                            @else
                                @php
                                    $customer = Session::get('customer');
                                @endphp
                                <div id="qc_left" class="aqc-column aqc-column-1" style="width:40%">
                                    <div id="step_2" data-sort="2" data-row="1" data-column="1" data-width="50"
                                         class="blocks" style="display: block;">
                                        <!-- Quick Checkout v4.0 by Dreamvention.com quickcheckout/register.tpl -->
                                        <div id="payment_address_wrap">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <span class="wrap"><span
                                                                class="fa fa-fw qc-icon-profile"></span></span>
                                                    <span class="text">Thông tin tài khoản</span>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="payment_address" class="form-horizontal ">
                                                        <div id="firstname_input"
                                                             class="text-input form-group  sort-item   required"
                                                             data-sort="1">
                                                            <div class="col-xs-6">
                                                                <label class="control-label"
                                                                       for="payment_address_firstname">
                                                                    <span class="text" title="">Tên:</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <input type="text" name="name"
                                                                       id="payment_address_firstname"
                                                                       data-require="require" data-refresh="0"
                                                                       value="{{$customer->name}}" class="form-control"
                                                                       autocomplite="off" placeholder="* Tên">
                                                            </div>
                                                        </div>
                                                        <div id="email_input"
                                                             class="text-input form-group  sort-item   required"
                                                             data-sort="3">
                                                            <div class="col-xs-6">
                                                                <label class="control-label"
                                                                       for="payment_address_email">
                                                                    <span class="text" title="">E-Mail:</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <input type="text" name="email"
                                                                       id="payment_address_email" data-require="require"
                                                                       data-refresh="0" value="{{$customer->email}}"
                                                                       class="form-control" autocomplite="off"
                                                                       placeholder="* E-Mail">
                                                            </div>
                                                        </div>
                                                        <div id="telephone_input"
                                                             class="text-input form-group  sort-item   required"
                                                             data-sort="4">
                                                            <div class="col-xs-6">
                                                                <label class="control-label"
                                                                       for="payment_address_telephone">
                                                                    <span class="text" title="">Điện thoại:</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <input type="text" name="phone"
                                                                       id="payment_address_telephone" data-require=""
                                                                       data-refresh="0" value="{{$customer->phone}}"
                                                                       class="form-control" autocomplite="off"
                                                                       placeholder=" Điện thoại">
                                                            </div>
                                                        </div>
                                                        <div id="address_1_input"
                                                             class="text-input form-group  sort-item   required"
                                                             data-sort="13">
                                                            <div class="col-xs-6">
                                                                <label class="control-label"
                                                                       for="payment_address_address_1">
                                                                    <span class="text" title="">Địa chỉ:</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <input type="text" name="address"
                                                                       id="payment_address_address_1"
                                                                       data-require="require" data-refresh="0"
                                                                       value="{{$customer->address}}"
                                                                       class="form-control" autocomplite="off"
                                                                       placeholder="* Địa chỉ">
                                                            </div>
                                                        </div>
                                                        {{--
                                                        <div id="newsletter_input" class="checkbox-input form-group checkbox  sort-item qc-hide  " data-sort="19">
                                                           <div class="col-xs-12">
                                                              <label for="payment_address_newsletter" class="control-label">
                                                              <input type="checkbox" name="payment_address[newsletter]" id="payment_address_newsletter" data-require="" data-refresh="0" value="1" checked="checked" class="styled" autocomplete="off">
                                                              <span class="text" title="">Đăng ký nhận bản tin Children Toys .</span>
                                                              </label>
                                                           </div>
                                                        </div>
                                                        --}}
                                                        {{--<div id="agree_input" class="checkbox-input form-group checkbox  sort-item   " data-sort="21">--}}
                                                        {{--<div class="col-xs-12">--}}
                                                        {{--<label for="payment_address_agree" class="control-label">--}}
                                                        {{--<input type="checkbox" name="payment_address[agree]" id="payment_address_agree" data-require="" data-refresh="0" value="0" class="styled" autocomplete="off">--}}
                                                        {{--<span class="text" title="">Tôi đã Đọc &amp; Đồng ý với <a href="http://9736.chilishop.net/index.php?route=information/information/agree&amp;information_id=3" class="agree"><b> Chính sách riêng tư</b></a></span>--}}
                                                        {{--</label>--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                        <div class="clear"></div>
                                                    </div>
                                                    <!-- #payment_address -->
                                                </div>
                                                <!-- .box-content -->
                                            </div>
                                            <!-- .box -->
                                        </div>
                                        <!-- #payment_address_wrap -->
                                    </div>
                                    <div id="step_3" data-sort="3" data-row="2" data-column="1" data-width="30"
                                         class="blocks" style="display: block;">
                                        <!-- Ajax Quick Checkout v4.2 by Dreamvention.com quickcheckout/register.tpl -->
                                        <div id="shipping_address_wrap">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <span class="wrap"><span
                                                                class="fa fa-fw qc-icon-shipping-address"></span></span>
                                                    <span class="text">Địa chỉ giao hàng</span>
                                                </div>
                                                <div class="panel-body">
                                                    <div>
                                                        <div class="radio-input">
                                                            <input type="radio" name="shipping_address"
                                                                   value="customer_info"
                                                                   checked="checked"
                                                                   class="styled" data-refresh="2" autocomplete="off">
                                                            <label for="shipping_address_exists_23">
                                                                {{ $customer->address }}
                                                            </label>
                                                        </div>
                                                        <div class="radio-input">
                                                            <input type="radio" name="shipping_address"
                                                                   value="receiver_info" id="shipping_address_exists_0"
                                                                   class="styled" data-refresh="2"
                                                                   autocomplete="off">
                                                            <label for="shipping_address_exists_0">Tôi muốn dùng địa chỉ
                                                                mới</label>
                                                        </div>
                                                    </div>
                                                    <div id="shipping_address" class="form-horizontal ">
                                                        <div id="firstname_input"
                                                             class="text-input form-group  sort-item   required"
                                                             data-sort="1">
                                                            <div class="col-xs-6">
                                                                <label class="control-label"
                                                                       for="shipping_address_firstname">
                                                                    <span class="text" title="">Tên người nhận:</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <input type="text" name="receiver_name"
                                                                       id="shipping_address_firstname"
                                                                       data-require="require" data-refresh="0"
                                                                       class="form-control"
                                                                       autocomplite="off" placeholder="* Tên">
                                                            </div>
                                                        </div>
                                                        <div id="address_1_input"
                                                             class="text-input form-group  sort-item   required"
                                                             data-sort="4">
                                                            <div class="col-xs-6">
                                                                <label class="control-label"
                                                                       for="shipping_address_address_1">
                                                                    <span class="text" title="">SĐT người
                                                                        nhận:</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <input type="text" name="receiver_phone"
                                                                       id="shipping_address_address_1"
                                                                       data-require="require" data-refresh="0"
                                                                        class="form-control"
                                                                       autocomplite="off" placeholder="* Số điện thoại">
                                                            </div>
                                                        </div>
                                                        <div id="address_1_input"
                                                             class="text-input form-group  sort-item   required"
                                                             data-sort="4">
                                                            <div class="col-xs-6">
                                                                <label class="control-label"
                                                                       for="shipping_address_address_1">
                                                                    <span class="text" title="">Địa chỉ người
                                                                        nhận:</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <input type="text" name="receiver_address"
                                                                       id="shipping_address_address_1"
                                                                       data-require="require" data-refresh="0"
                                                                        class="form-control"
                                                                       autocomplite="off" placeholder="* Địa chỉ">
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>

                                                </div><!-- /.box-content -->
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $(document).ready(function() {
                                                $('#shipping_address').hide();
                                                $('#shipping_address_exists_0').change(function() {
                                                    if(this.checked) {
                                                        $('#shipping_address').show();
                                                    }
                                                })
                                                $('#payment_wrap').hide();
                                                $('#bank_transfer').change(function() {
                                                    if(this.checked) {
                                                        $('#payment_wrap').show();
                                                    }
                                                })
                                            })
                                            // $('input[name=\'shipping_address[exists]\']').live('click', function() {
                                            // 	if (this.value == '0') {
                                            // 		$('#shipping_address_exists_list').hide();
                                            // 		$('#shipping_address').show();
                                            // 	} else {
                                            // 		$('#shipping_address_exists_list').show();
                                            // 		$('#shipping_address').hide();
                                            // 	}
                                            // });
                                            function refreshShippingAddessZone(value) {
                                                $.ajax({
                                                    url: 'index.php?route=module/d_quickcheckout/country&country_id=' + value,
                                                    dataType: 'json',
                                                    success: function (json) {

                                                        html = '<option value=""> --- Chọn --- </option>';

                                                        if (json['zone'] != '') {
                                                            for (i = 0; i < json['zone'].length; i++) {
                                                                html += '<option value="' + json['zone'][i]['zone_id'] + '"';
                                                                if (json['zone'][i]['zone_id'] == '3765') {
                                                                    html += ' selected="selected"';
                                                                }
                                                                html += '>' + json['zone'][i]['name'] + '</option>';
                                                            }
                                                        } else {
                                                            html += '<option value="0" selected="selected"> --- Không --- </option>';
                                                        }

                                                        $('#shipping_address_wrap select[name=\'shipping_address[zone_id]\']').html(html);
                                                    },
                                                    error: function (xhr, ajaxOptions, thrownError) {
                                                        console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                                    }
                                                });
                                            }

                                            $('#shipping_address_wrap select[name=\'shipping_address[country_id]\']').bind('change', function () {
                                                refreshShippingAddessZone($(this).val())
                                            })

                                            $(function () {
                                                if ($.isFunction($.fn.uniform)) {
                                                    $(" .styled, input:radio.styled").uniform().removeClass('styled');
                                                }
                                                if ($.isFunction($.fn.colorbox)) {
                                                    $('.colorbox').colorbox({
                                                        width: 640,
                                                        height: 480
                                                    });
                                                }
                                                if ($.isFunction($.fn.fancybox)) {
                                                    $('.fancybox').fancybox({
                                                        width: 640,
                                                        height: 480
                                                    });
                                                }
                                            });
                                            //--></script>
                                        <script>(function (c) {
                                                if (c && c.groupCollapsed) {
                                                    c.log("%c%cd_quickcheckout%c %cDEBUG%c load_settings() (3ms)", "font-weight: normal", "font-weight: bold", "font-weight: normal", "background-color: blue; color: white; border-radius: 3px; padding: 0 2px 0 2px", "font-weight: normal");
                                                    c.log("%c%cd_quickcheckout%c %cDEBUG%c get_shipping_methods() (11ms)", "font-weight: normal", "font-weight: bold", "font-weight: normal", "background-color: blue; color: white; border-radius: 3px; padding: 0 2px 0 2px", "font-weight: normal");
                                                    c.log("%c%cd_quickcheckout%c %cDEBUG%c update_order() (2ms)", "font-weight: normal", "font-weight: bold", "font-weight: normal", "background-color: blue; color: white; border-radius: 3px; padding: 0 2px 0 2px", "font-weight: normal");
                                                    c.log("%c%cd_quickcheckout%c %cDEBUG%c get_total_data() (0ms)", "font-weight: normal", "font-weight: bold", "font-weight: normal", "background-color: blue; color: white; border-radius: 3px; padding: 0 2px 0 2px", "font-weight: normal");
                                                    c.log("%c%cd_quickcheckout%c %cDEBUG%c get_shipping_address_view() (11ms)", "font-weight: normal", "font-weight: bold", "font-weight: normal", "background-color: blue; color: white; border-radius: 3px; padding: 0 2px 0 2px", "font-weight: normal");
                                                }
                                            })(console);</script>
                                    </div>
                                </div>
                                <div id="qc_right" style="width:60%; float:left">
                                    <div class="aqc-column aqc-column-2" style="width:50%">
                                        <div id="step_4" data-sort="4" data-row="1" data-column="2" data-width="30"
                                             class="blocks" style="display: block;">
                                            <!-- Quick Checkout v4.0 by Dreamvention.com quickcheckout/shipping_method.tpl -->
                                            <div id="shipping_method_wrap">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <span class="wrap">
                                                            <span class="fa fa-fw qc-icon-shipping-method"></span>
                                                        </span>
                                                        <span class="text">Phương thức vận chuyển</span>
                                                    </div>
                                                    <div class="panel-body">
                                                        <p class="description">Hãy chọn phương thức Vận chuyển:</p>
                                                        <div class="">
                                                            <div class="title">Phí cố định</div>
                                                            <div class="radio-input radio">
                                                                <label for="flat.flat">
                                                                    <input type="radio" name="shipping_method"
                                                                           value="flat.flat" id="flat.flat"
                                                                           checked="checked" data-refresh="5"
                                                                           class="styled">
                                                                    <span class="text">Phí vận chuyển cố
                                                                        định</span><span class="price">20.000 VNĐ</span></label>
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="aqc-column aqc-column-3" style="width:50%">
                                        <div id="step_5" data-sort="5" data-row="1" data-column="3" data-width="30"
                                             class="blocks" style="display: block;">
                                            <!-- Quick Checkout v4.0 by Dreamvention.com quickcheckout/payment_method.tpl -->
                                            <div id="payment_method_wrap">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading ">
                                                        <span class="wrap"><span
                                                                    class="fa fa-fw qc-icon-payment-method"></span></span>
                                                        <span class="text">Phương thức thanh toán</span>
                                                    </div>
                                                    <div class="panel-body">
                                                        <p class="description">Hãy chọn phương thức Thanh toán:</p>
                                                        <div class="payment-methods ">
                                                            <div class="radio-input radio">
                                                                <label for="cod">
                                                                    <input type="radio" name="payment_method"
                                                                           value="cod" id="cod" checked="checked"
                                                                           class="styled" data-refresh="6">
                                                                    <img class="payment-image "
                                                                         src="resources/views/image/data/payment/cod.png">
                                                                    Thu tiền khi giao hàng<span
                                                                            class="price"></span></label>
                                                            </div>
                                                            <div class="radio-input radio">
                                                                <label for="bank_transfer">
                                                                    <input type="radio" name="payment_method"
                                                                           value="bank_transfer" id="bank_transfer"
                                                                           class="styled" data-refresh="6">
                                                                    <img class="payment-image "
                                                                         src="resources/views/image/data/payment/bank_transfer.png">
                                                                    Chuyển khoản ngân hàng<span
                                                                            class="price"></span></label>
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="aqc-column aqc-column-4" style="width:100%">
                                        <div id="step_6" data-sort="6" data-row="2" data-column="4" data-width="50"
                                             class="blocks" style="display: block;">
                                            <!-- Quick Checkout v4.2 by Dreamvention.com quickcheckout/cart.tpl -->
                                            <style>
                                                .qc.qc-popup {
                                                    width: 152px;
                                                    height: 152px;
                                                }
                                            </style>
                                            <div id="cart_wrap">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading ">
                                                        <span class="wrap"><span class="qc-icon-cart"></span></span>
                                                        <span class="text">Giỏ Hàng</span>
                                                    </div>
                                                    <div class="qc-checkout-product panel-body ">
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered qc-cart">
                                                                <thead>
                                                                <tr>
                                                                    <td class="qc-image ">Hình ảnh:</td>
                                                                    <td class="qc-name ">Tên sản phẩm:</td>
                                                                    <td class="qc-model qc-hide">Dòng sản phẩm:</td>
                                                                    <td class="qc-quantity ">Số lượng:</td>
                                                                    <td class="qc-price   ">Đơn giá:</td>
                                                                    <td class="qc-total ">Tổng Cộng:</td>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach ($cart->items as $item)
                                                                    <tr>
                                                                        <td class="qc-image  ">
                                                                            <a href="product?id={{$item->id}}"
                                                                               data-container="body"
                                                                               data-toggle="popover"
                                                                               data-placement="top"
                                                                               data-content="<img src='uploads/{{$item->featured_image}}' />"
                                                                               data-trigger="hover">
                                                                                <img src="uploads/{{$item->featured_image}}">
                                                                            </a>
                                                                            <i rel="tooltip"
                                                                               data-help="'.$field['tooltip'] .'"></i>
                                                                        </td>
                                                                        <td class="qc-name   ">
                                                                            <a href="product?id={{$item->id}}">
                                                                                {{$item->title}}            </a>
                                                                            <div class="qc-name-model qc-hide"><span
                                                                                        class="title">Mã sản
                                                                                    phẩm:</span> <span
                                                                                        class="text">{{$item->id}}</span>
                                                                            </div>
                                                                            <div class="qc-name-price "><span
                                                                                        class="title">Đơn giá:</span>
                                                                                <span class="text">{{Helper::formatMoney($item->price)}}
                                                                                    VNĐ</span></div>
                                                                        </td>
                                                                        <td class="qc-model qc-hide ">{{$item->id}}</td>
                                                                        <td class="qc-quantity   ">
                                                                            <div class="input-group">
                                                                                <span class="input-group-btn">
                                                                                    <button class="btn btn-defaut  btn-minus"
                                                                                            name="minus"
                                                                                            data-product="YToyOntzOjEwOiJwcm9kdWN0X2lkIjtpOjM7czo2OiJvcHRpb24iO2E6MTp7aToxO3M6MToiNCI7fX0=">
                                                                                        <i class="fa fa-minus"></i>
                                                                                    </button>
                                                                                </span>
                                                                                <input type="text"
                                                                                       value="{{$item->quantity}}"
                                                                                       class="qc-product-qantity form-control text-center"
                                                                                       data-id="{{$item->id}}"
                                                                                       name="quantity" data-refresh="2">
                                                                                <span class="input-group-btn">
                                                                                    <button class="btn btn-defaut  btn-plus"
                                                                                            name="plus"
                                                                                            data-product="YToyOntzOjEwOiJwcm9kdWN0X2lkIjtpOjM7czo2OiJvcHRpb24iO2E6MTp7aToxO3M6MToiNCI7fX0=">
                                                                                        <i class="fa fa-plus"></i>
                                                                                    </button>
                                                                                </span>
                                                                            </div>
                                                                        </td>
                                                                        <td class="qc-price  ">{{Helper::formatMoney($item->price)}}
                                                                            VNĐ
                                                                        </td>
                                                                        <td class="qc-total  ">{{Helper::formatMoney($item->price * $item->quantity)}}
                                                                            VNĐ
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="form-horizontal qc-options">
                                                            {{--
                                                            <div class="row form-group qc-coupon ">
                                                               <label class="col-sm-6 control-label">
                                                               Sử dụng Mã giảm Giá          </label>
                                                               <div class="col-sm-6 qc-total">
                                                                  <div class="input-group">
                                                                     <input type="text" value="" name="coupon" id="coupon" placeholder="Sử dụng Mã giảm Giá" class="form-control">
                                                                     <span class="input-group-btn">
                                                                     <button class="btn btn-primary" id="confirm_coupon" type="button"><i class="fa fa-check"></i></button>
                                                                     </span>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            --}}
                                                            <div class="row form-group qc-voucher ">
                                                                <label class="col-sm-6 control-label">
                                                                    Sử dụng Phiếu Voucher </label>
                                                                <div class="col-sm-6 qc-total">
                                                                    <div class="input-group">
                                                                        <input type="text" value="" name="voucher"
                                                                               id="voucher"
                                                                               placeholder="Sử dụng Phiếu Voucher"
                                                                               class="form-control">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn btn-primary"
                                                                                    id="confirm_voucher" type="button">
                                                                                <i class="fa fa-check"></i></button>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group qc-reward qc-hide">
                                                                <label class="col-sm-6 control-label">
                                                                    Use Reward Points (Available ) </label>
                                                                <div class="col-sm-6 qc-total ">
                                                                    <div class="input-group">
                                                                        <input type="text" value="" name="reward"
                                                                               id="reward"
                                                                               placeholder="Use Reward Points (Available )"
                                                                               class="form-control">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn btn-primary"
                                                                                    id="confirm_reward" type="button"><i
                                                                                        class="fa fa-check"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-horizontal qc-summary ">
                                                            <div class="row qc-totals">
                                                                <label class="col-xs-6 control-label">Thành tiền</label>
                                                                <div class="col-xs-6 form-control-static">{{Helper::formatMoney($cart->total)}}
                                                                    VNĐ
                                                                </div>
                                                            </div>
                                                            <div class="row qc-totals">
                                                                <label class="col-xs-6 control-label">Giảm giá:</label>
                                                                <div class="col-xs-6 form-control-static">0 VNĐ</div>
                                                            </div>
                                                            <div class="row qc-totals">
                                                                <label class="col-xs-6 control-label">Phí vận chuyển cố
                                                                    định</label>
                                                                <div class="col-xs-6 form-control-static">20.000 VNĐ
                                                                </div>
                                                            </div>
                                                            <div class="row qc-totals">
                                                                <label class="col-xs-6 control-label">Tổng cộng </label>
                                                                <div class="col-xs-6 form-control-static">{{Helper::formatMoney($cart->total + 20000)}}
                                                                    VNĐ
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="step_7" data-sort="7" data-row="2" data-column="4" data-width="50"
                                             class="blocks" style="display: block;">
                                            <style>
                                                #payment_input select {
                                                    width: inherit;
                                                }</style>
                                            <div id="payment_wrap">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div id="payment_input">
                                                            <div id="confirm_payment"><h2>HƯỚNG DẪN THANH TOÁN QUA NGÂN
                                                                    HÀNG</h2>
                                                                <p><b><font color="GREEN">Bạn vui lòng chuyển tiền Mua
                                                                            hàng và tiền Cước vận chuyển (nếu có) vào 1
                                                                            trong các tài khoản Ngân hàng
                                                                            sau:</font></b></p>
                                                                <div class="well well-sm">
                                                                    <p>ABC Á Châu<br>
                                                                        Tên chủ tài khoản: Nguyễn Văn A<br>
                                                                        Số tài khoản:5734953453495345</p>
                                                                    <p>Sau khi chuyển tiền, Quý khách liên hệ bộ phận
                                                                        chăm sóc khách hàng để được xác nhận đã thanh
                                                                        toán xong tiền hàng và tiền cước vận chuyển (nếu
                                                                        có).</p>
                                                                </div>
                                                                <div class="buttons">
                                                                    <div class="pull-right">
                                                                        <input type="button" value="Xác nhận đơn hàng"
                                                                               id="button-confirm"
                                                                               class="btn btn-primary"
                                                                               data-loading-text="Đang Xử lý...">
                                                                    </div>
                                                                </div>
                                                                <script type="text/javascript"><!--
                                                                    $('#button-confirm').on('click', function () {
                                                                        $.ajax({
                                                                            type: 'get',
                                                                            url: 'index.php?route=payment/bank_transfer/confirm',
                                                                            cache: false,
                                                                            beforeSend: function () {
                                                                                $('#button-confirm').button('loading');
                                                                            },
                                                                            complete: function () {
                                                                                $('#button-confirm').button('reset');
                                                                            },
                                                                            success: function () {
                                                                                location = 'http://9736.chilishop.net/index.php?route=checkout/success';
                                                                            }
                                                                        });
                                                                    });
                                                                    //--></script>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <script>(function (c) {
                                                    if (c && c.groupCollapsed) {
                                                        c.log("%c%cd_quickcheckout%c %cDEBUG%c load_settings() (3ms)", "font-weight: normal", "font-weight: bold", "font-weight: normal", "background-color: blue; color: white; border-radius: 3px; padding: 0 2px 0 2px", "font-weight: normal");
                                                        c.log("%c%cd_quickcheckout%c %cDEBUG%c get_shipping_methods() (11ms)", "font-weight: normal", "font-weight: bold", "font-weight: normal", "background-color: blue; color: white; border-radius: 3px; padding: 0 2px 0 2px", "font-weight: normal");
                                                        c.log("%c%cd_quickcheckout%c %cDEBUG%c update_order() (2ms)", "font-weight: normal", "font-weight: bold", "font-weight: normal", "background-color: blue; color: white; border-radius: 3px; padding: 0 2px 0 2px", "font-weight: normal");
                                                        c.log("%c%cd_quickcheckout%c %cDEBUG%c get_total_data() (0ms)", "font-weight: normal", "font-weight: bold", "font-weight: normal", "background-color: blue; color: white; border-radius: 3px; padding: 0 2px 0 2px", "font-weight: normal");
                                                        c.log("%c%cd_quickcheckout%c %cDEBUG%c get_payment_view() (10ms)", "font-weight: normal", "font-weight: bold", "font-weight: normal", "background-color: blue; color: white; border-radius: 3px; padding: 0 2px 0 2px", "font-weight: normal");
                                                    }
                                                })(console);</script>
                                        </div>
                                        <div id="step_8" data-sort="8" data-row="2" data-column="4" data-width="50"
                                             class="blocks" style="display: block;">
                                            <!-- Quick Checkout v4.0 by Dreamvention.com quickcheckout/cofirm.tpl -->
                                            <div id="confirm_wrap">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div id="confirm_inputs" class="form-horizontal">
                                                            <div id="comment_input"
                                                                 class="textarea-input form-group sort-item   "
                                                                 data-sort="">
                                                                <div class="col-xs-12">
                                                                    <label class="control-label" for="confirmcomment">
                                                                        <span class="text" title="">Ghi chú:</span>
                                                                    </label>
                                                                    <textarea name="confirm[comment]"
                                                                              id="confirm_comment" data-require=""
                                                                              data-refresh="0" class="form-control"
                                                                              placeholder="Hãy để lại lời nhắn cho chúng tôi"></textarea>
                                                                </div>
                                                            </div>
                                                            {{-- <div id="agree_input" class="checkbox-input form-group checkbox  sort-item   required" data-sort="">
                                                               <div class="col-xs-12">
                                                                  <label for="confirm_agree" class="control-label">
                                                                  <input type="checkbox" name="confirm[agree]" id="confirm_agree" data-require="require" data-refresh="0" value="0" class="styled" autocomplete="off">
                                                                  <span class="text" title="">Tôi đã Đọc &amp; Đồng ý với <a href="http://9736.chilishop.net/index.php?route=information/information/agree&amp;information_id=5" class="agree"><b> Điều khoản và điều kiện</b></a></span>
                                                                  </label>
                                                               </div>
                                                            </div> --}}
                                                            <div class="clear"></div>
                                                        </div>
                                                        <!-- #confirm_inputs -->
                                                        <div>
                                                            <div class="buttons">
                                                                <div class="right">
                                                                    <input type="button" id="confirm-cart"
                                                                           class="button btn btn-primary"
                                                                           value="Xác nhận đơn hàng">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <script>
                                                $(function () {
                                                    if ($.isFunction($.fn.uniform)) {
                                                        $(" .styled, input:radio.styled").uniform().removeClass('styled');
                                                    }
                                                    if ($.isFunction($.fn.colorbox)) {
                                                        $('.colorbox').colorbox({
                                                            width: 640,
                                                            height: 480
                                                        });
                                                    }
                                                    if ($.isFunction($.fn.fancybox)) {
                                                        $('.fancybox').fancybox({
                                                            width: 640,
                                                            height: 480
                                                        });
                                                    }
                                                });
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="position-display">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('input[name=quantity]').on('change', function (e) {
            e.preventDefault();
            var main = $(this);
            var id = $(this).data('id');
            var quantity = $(this).val();
            StoreAPI.changeItem(id, quantity, function (result) {
                main.parent().parent().next().text(money(result.data.price) + " VNĐ");
                main.parent().parent().next().next().text(money(result.data.price * result.data.quantity) + " VNĐ");
            })
        });
        $('.btn-minus').on('click', function (e) {
            e.preventDefault();
            if ($(this).parent().next().val() > 1)
                $(this).parent().next().val($(this).parent().next().val() - 1);
            $(this).parent().next().trigger('change');
        });
        $('.btn-plus').on('click', function (e) {
            e.preventDefault();
            $(this).parent().prev().val(parseInt($(this).parent().prev().val()) + 1);
            $(this).parent().prev().trigger('change');
        });
        $('#confirm-cart').on('click', function (e) {
            e.preventDefault();
                    var customer = {
                        name: $('input[name=name]').val(),
                        email: $('input[name=email]').val(),
                        phone: $('input[name=phone]').val(),
                        address: $('input[name=address]').val(),

                        receiver_name: $('input[name=receiver_name]').val(),
                        receiver_phone: $('input[name=receiver_phone]').val(),
                        receiver_address: $('input[name=receiver_address]').val()
                    }
                    if ($('input[name=shipping_address]:checked').val() === 'customer_info') {
                        customer.receiver_name = customer.name;
                        customer.receiver_phone = customer.phone;
                        customer.receiver_address = customer.address;
                    }
                    var payment_method = $('input[name=payment_method]:checked').val();
                    var params = {
                        type: 'POST',
                        url: 'checkout',
                        data: {
                            customer_name: customer.name,
                            customer_email: customer.email,
                            customer_phone: customer.phone,
                            customer_address: customer.address,
                            receiver_name: customer.receiver_name,
                            receiver_phone: customer.receiver_phone,
                            receiver_address: customer.receiver_address,
                            payment_method: payment_method
                        },
                        dataType: 'json',
                        success: function (result) {
                            if (result.status == 'success') {
                                window.location.href = "success";
                            }
                        }
                    }
                    $.ajax(params);
        })

    </script>
@endsection
