@extends('master')
@section('content')
    <div class="breadcrumb-wrapper">
        <div class="breadcrumb-title">
            <h1 class="page-title"><span>Thông tin tài khoản</span></h1>
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="http://9736.chilishop.net/index.php?route=common/home">Trang chủ</a></li>
                    <li class="active"><a href="http://9736.chilishop.net/index.php?route=account/account">Tài khoản</a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <div class="container">
        @if (!empty($action_result))
        <div class="alert alert-success"><i class="fa fa-check-circle"></i>
                {{$action_result}}
        </div>
        @endif
        <div class="row">
            <div id="content" class="col-sm-12">
                <div class="position-display">
                </div>
                <h2>Tài khoản của tôi</h2>
                <ul class="list-unstyled">
                    <li><a href="/changeinfo">Cập nhật thông tin tài
                            khoản</a>
                    </li>
                    <li><a href="/changepasswd">Thay đổi mật khẩu</a></li>
                    <li><a href="/changeaddress">Cập nhật địa chỉ</a></li>
                </ul>
                <h2>Đơn hàng của tôi</h2>
                <ul class="list-unstyled">
                    <li><a href="./listorder">Xem lịch sử Đặt hàng</a></li>
                    <li><a href="http://9736.chilishop.net/index.php?route=account/return">Đổi / Trả hàng</a></li>
                    <li><a href="http://9736.chilishop.net/index.php?route=account/transaction">Lịch sử Giao dịch</a>
                    </li>
                </ul>
                <h2>Thư thông báo</h2>
                <ul class="list-unstyled">
                    <li><a href="http://9736.chilishop.net/index.php?route=account/newsletter">Đăng ký / Hủy đăng ký
                            thông
                            báo</a></li>
                </ul>
                <div class="position-display">
                </div>
            </div>
        </div>
    </div>
@endsection