@extends('master')
@section('content')

    <body class="account-order-info">
    <div class="breadcrumb-wrapper">
        <div class="breadcrumb-title">
            <h1 class="page-title"><span>Thông tin đơn hàng</span></h1>
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="http://9736.chilishop.net/index.php?route=common/home">Trang chủ</a></li>
                    <li><a href="http://9736.chilishop.net/index.php?route=account/account">Tài khoản</a></li>
                    <li><a href="http://9736.chilishop.net/index.php?route=account/order">Lịch Sử Đặt Hàng</a></li>
                    <li class="active">
                        <a href="http://9736.chilishop.net/index.php?route=account/order/info&amp;order_id=29">Thông tin đơn hàng</a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-12">
                <div class="position-display"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-left" colspan="2">Chi tiết đơn hàng</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left" style="width: 50%;">
                                @foreach($order_id_created_at as $item)
                                    <b>Mã đơn hàng:</b> {{$item->order_id}}<br>
                                    <b>Ngày tạo:</b> {{$item->created_at}}
                                @endforeach
                            </td>
                            <td class="text-left">
                                <b>Phương thức thanh toán:</b> Thu tiền khi giao hàng<br>
                                <b>Phương thức vận chuyển:</b> Phí vận chuyển cố định
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-left" style="width: 50%;">Thông tin tài khoản</td>
                            <td class="text-left">Địa chỉ giao hàng</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left">
                                @foreach($data_cus as $cus)
                                    {{$cus->name}}<br>
                                    {{$cus->address}}<br>
                                    {{$cus->phone}}<br>
                                @endforeach
                            </td>
                            <td class="text-left">
                                @foreach($info_receiver as $receiver)
                                    {{$receiver->receiver_name}}<br>
                                    {{$receiver->receiver_address}}<br>
                                    {{$receiver->receiver_phone}}<br>
                                @endforeach
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-left">Tên sản phẩm</td>
                            <td class="text-left">Dòng sản phẩm</td>
                            <td class="text-right">Số lượng</td>
                            <td class="text-right">Đơn Giá</td>
                            <td class="text-right">Tổng Cộng</td>
                            <td style="width: 20px;"></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($info_product as $product)
                        <tr>
                            <td class="text-left">{{$product->title}}<br>
                                &nbsp;<!--small> - Kích thước: </small-->
                            </td>
                            <td class="text-left">{{$product->collection_id}}</td>
                            <td class="text-right">{{$product->quantity}}</td>
                            <td class="text-right">{{$product->price}}</td>
                            <td class="text-right">{{$product->price * $product->quantity}}</td>
                            <td class="text-right" style="white-space: nowrap;">
                                <a href="http://9736.chilishop.net/index.php?route=account/order/reorder&amp;
                                order_id=29&amp;order_product_id=3645"
                                   data-toggle="tooltip"
                                   title="" class="btn btn-primary"
                                   data-original-title="Đặt hàng lại">
                                    <i class="fa fa-shopping-cart"></i></a>
                                <a href="http://9736.chilishop.net/index.php?route=account/return/add&amp;
                                order_id=29&amp;product_id=20" data-toggle="tooltip"
                                   title="" class="btn btn-danger"
                                   data-original-title="Đổi / Trả Hàng">
                                    <i class="fa fa-reply"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right">
                                <b>Thành tiền</b>
                            </td>
                            <td class="text-right">
                                @if(Session::has('sum'))
                                    {{ Session::get('sum')}}
                                @endif
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right">
                                <b>Điểm thưởng():</b>
                            </td>
                            <td class="text-right"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right">
                                <b>Phí vận chuyển cố định</b>
                            </td>
                            <td class="text-right">
                                @if(Session::has('shipping_fee'))
                                    {{ Session::get('shipping_fee')}}
                                @endif</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right">
                                <b>Tổng cộng </b>
                            </td>
                            <td class="text-right">
                                @if(Session::has('total'))
                                    {{ Session::get('total')}}
                                @endif</td></td>
                            <td></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <h3>Lịch sử đơn hàng</h3>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-left">Ngày tạo</td>
                            <td class="text-left">Tình trạng</td>
                            <td class="text-left">Ghi chú</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left"></td>
                            <td class="text-left"></td>
                            <td class="text-left"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="buttons clearfix button-box">
                    <div class="pull-right">
                        <a href="http://9736.chilishop.net/index.php?route=account/order"
                           class="btn btn-primary">Tiếp tục</a></div>
                </div>
                <div class="position-display">
                </div></div>
        </div>
    </div>
    <div class="sticky-bottom">
        <div id="pcSupport" class="wrap">
            <a href="tel:19007179">Hotline 24/7: 1900 7179</a>
        </div>
    </div>
    <a href="tel:19007179" mypage="" class="call-now" rel="nofollow">
        <div class="mypage-alo-phone">
            <div class="animated infinite zoomIn mypage-alo-ph-circle"></div>
            <div class="animated infinite pulse mypage-alo-ph-circle-fill"></div>
            <div class="animated infinite tada mypage-alo-ph-img-circle"></div>
        </div>
    </a>

@endsection