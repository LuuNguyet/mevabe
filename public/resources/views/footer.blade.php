<footer>
    <div class="footer-top" id="footer_top">
        <div class="container">
            <column class="position-display">
                <div>
                    <div class="dv-builder-full">
                        <div class="dv-builder  vertical_footer">
                            <div class="dv-module-content">
                                <div class="row">
                                    <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12">
                                        <div class="dv-item-module ">
                                            <div class="content_info footer_contact">
                                                <ul class="list">
                                                    <li class="item-content">
                                                        @php
                                                            $shop_name = Helper::getMeta('shop-name');
                                                        @endphp
                                                        <div class="item-title">
                                                            {{ $shop_name }} <span class="title-child"></span>
                                                        </div>
                                                        @php
                                                            $slogan = Helper::getMeta('slogan');
                                                        @endphp
                                                        <div class="item-description">
                                                                {{ $slogan }}
                                                        </div>
                                                    </li>
                                                    <li class="item-content">
                                                        @php
                                                            $address = Helper::getMeta('address');
                                                        @endphp
                                                        <div class="item-description">
                                                                {{ $address }}
                                                        </div>
                                                    </li>
                                                    <li class="item-content">
                                                        <div class="item-image">
                                                            <img src="resources/views/image/cache/catalog/banner/paypal-190x28.png">
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12">
                                        <div class="dv-item-module ">
                                            <div class="close-header-layer"></div>
                                            <div class="menu_vertical" id="menu_id_footer1">
                                                <div class="vertical-name">
                                                    <h4 class="title">
                                                        <span class="btn-open-mobile"><i class="fa fa-bars"></i></span>
                                                        <a href="javascript:;" class="title-menu">
                                                            Thông tin </a>
                                                    </h4>
                                                    <nav class="menu">
                                                        <li class="vertical-menu-list">
                                                            <a href="/mevabe/resources/chinh-sach-rieng-tu.html">

                                                                Chính sách riêng tư </a>
                                                        </li>

                                                        <li class="vertical-menu-list">
                                                            <a href="/mevabe/resources/thao-thuan-nguoi-dung.html">

                                                                Thỏa thuận người dùng </a>
                                                        </li>

                                                        <li class="vertical-menu-list">
                                                            <a href="/mevabe/resources/dieu-khoang-dieu-kien.html">

                                                                Điều khoản &amp; Điều kiện </a>
                                                        </li>

                                                        <li class="vertical-menu-list">
                                                            <a href="/mevabe/resources/gioi-thieu.html">

                                                                Về chúng tôi </a>
                                                        </li>

                                                    </nav>

                                                </div>
                                            </div>
                                            <script>
                                                $(function () {
                                                    window.prettyPrint && prettyPrint()
                                                    $(document).on('click', '.navbar .dropdown-menu', function (e) {
                                                        e.stopPropagation()
                                                    })
                                                })
                                            </script>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12">
                                        <div class="dv-item-module ">
                                            <div class="close-header-layer"></div>
                                            <div class="menu_vertical" id="menu_id_tai_khoan">
                                                <div class="vertical-name">
                                                    <h4 class="title">
                                                        <span class="btn-open-mobile"><i class="fa fa-bars"></i></span>
                                                        <a href="javascript:;" class="title-menu">
                                                            Tài khoản </a>
                                                    </h4>
                                                    <nav class="menu">
                                                        <li class="vertical-menu-list">
                                                            <a href="?route=account/account">

                                                                Tài khoản </a>
                                                        </li>

                                                        <li class="vertical-menu-list">
                                                            <a href="?route=account/order">

                                                                Đơn hàng </a>
                                                        </li>

                                                        <li class="vertical-menu-list">
                                                            <a href="?route=account/wishlist">

                                                                Yêu thích </a>
                                                        </li>

                                                        <li class="vertical-menu-list">
                                                            <a href="?route=account/newsletter">

                                                                Thông báo </a>
                                                        </li>

                                                    </nav>

                                                </div>
                                            </div>
                                            <script>
                                                $(function () {
                                                    window.prettyPrint && prettyPrint()
                                                    $(document).on('click', '.navbar .dropdown-menu', function (e) {
                                                        e.stopPropagation()
                                                    })
                                                })
                                            </script>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12">
                                        <div class="dv-item-module ">
                                            <div class="close-header-layer"></div>
                                            <div class="menu_vertical" id="menu_id_dich_vu">
                                                <div class="vertical-name">
                                                    <h4 class="title">
                                                        <span class="btn-open-mobile"><i class="fa fa-bars"></i></span>
                                                        <a href="javascript:;" class="title-menu">
                                                            Dịch vụ </a>
                                                    </h4>
                                                    <nav class="menu">
                                                        <li class="vertical-menu-list">
                                                            <a href="/contact">

                                                                Liên hệ </a>
                                                        </li>
                                                        <li class="vertical-menu-list">
                                                            <a href="?route=information/sitemap">

                                                                Sơ đồ trang </a>
                                                        </li>
                                                    </nav>

                                                </div>
                                            </div>
                                            <script>
                                                $(function () {
                                                    window.prettyPrint && prettyPrint()
                                                    $(document).on('click', '.navbar .dropdown-menu', function (e) {
                                                        e.stopPropagation()
                                                    })
                                                })
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </column>
        </div>
    </div>
    <div class="footer-bottom" id="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-xs-12 copyright text-left">
                    <column class="position-display">
                        <div>
                            <div class="dv-builder-full">
                                <div class="dv-builder ">
                                    <div class="dv-module-content">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                                <div class="dv-item-module ">
                                                    @php
                                                        $copyright = Helper::getMeta('copyright');
                                                    @endphp
                                                    <div>
                                                        © 2016 Copyright by {{ $copyright }} – Số ĐKKD 1111 – Tại sở Kế
                                                        Hoạch và Đầu Tư TP.HCM – (<a href="/chinh-sach-rieng-tu.html"
                                                                                     class="provision"
                                                                                     style="margin: 0px; padding: 0px;">Chính
                                                            sách riêng tư</a>)
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </column>
                </div>
                <div class="col-sm-4 col-xs-12 chili text-right">
                    <div style="white-space:nowrap;display: block;overflow: hidden;">
                        <a style="color:#000000;font-size:12px;background: url(https://www.chili.vn/chili_vn_web_doanh_nghiep.png);
    background-repeat: no-repeat; background-position: right; padding-right: 37px;" target="_blank"
                           href="https://www.chili.vn/kho-giao-dien-web-ban-hang">Thiết kế bởi</a>
                        <!--     <a target="_blank" style="border:0px; line-height:14px; font-size: 12px;overflow: hidden; " href="https://www.chili.vn"><img style="" src="https://www.chili.vn/chili_vn_web_doanh_nghiep.png" alt="Dịch vụ thiết kế web chuyên biệt dành cho Doanh Nghiệp, Shop Bán hàng và nhà Quảng Cáo" /></a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
