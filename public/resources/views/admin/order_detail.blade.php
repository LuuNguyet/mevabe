@extends('admin.layout')
@section('header') Đơn hàng
@endsection
@section('content')
    <style media="screen">
        .block {
            margin-bottom: 20px;
        }
    </style>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-8 block">
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="col-xs-12 title">Thông tin giỏ hàng</div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="mytable" class="table table-bordred table-striped">
                                    <thead>
                                    <th style="width:20%">Tên sản phẩm</th>
                                    <th>Số lượng</th>
                                    <th>Giá gốc</th>
                                    <th>Giá mua</th>
                                    <th>Tổng cộng</th>
                                    </thead>
                                    <tbody>
                                    @foreach ($order->items as $item)
                                        <tr class="item" id="{{$order->id}}">
                                            <td>
                                                <a href="/admin/product?id={{$item->product->id}}">{{$item->product->title}}</a>
                                            </td>
                                            <td>{{$item->quantity}} </td>
                                            <td>{{Helper::formatMoney($item->product->price)}} VNĐ</td>
                                            <td>{{Helper::formatMoney($item->unit_price)}} VNĐ</td>
                                            <td>{{Helper::formatMoney($item->unit_price * $item->quantity)}} VNĐ</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="4" class="text-right"><strong>Tạm tính</strong></td>
                                        <td><strong>{{Helper::formatMoney($order->total)}} VNĐ</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="text-right"><strong>Tổng cộng</strong></td>
                                        <td><strong>{{Helper::formatMoney($order->total)}} VNĐ</strong></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 block">
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="col-xs-12 title">Trạng thái đơn hàng</div>
                    </div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="form-group">
                                <div class="col-sm-6 text-left">Mã đơn hàng:</div>
                                <div class="col-sm-6">{{$order->id}}</div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 text-left">Phương thức thanh toán:</div>
                                <div class="col-sm-6">
                                    <label class="label label-info">{{$order->payment_method}}</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 text-left">Trạng thái:</div>
                                <div class="col-sm-6">
                                    <select class="" name="order_status" class="form-control" data-id="{{$order->id}}">
                                        @php
                                            $status = ['new', 'confirmed', 'paid', 'cancel'];
                                            $title = ['Mới', 'Đã xác nhận', 'Đã thanh toán', 'Đã hủy'];
                                        @endphp
                                        @for ($i=0; $i < 4; $i++)
                                            <option value="{{$status[$i]}}"
                                                    @if ($order->order_status == $status[$i])
                                                    selected="selected"
                                                    @endif
                                            >{{$title[$i]}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 text-left">Ngày tạo:</div>
                                <div class="col-sm-6"><strong>{{$order->created_at}}</strong></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8 block">
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="col-xs-12 title">THÔNG TIN KHÁCH HÀNG
                            <span><a data-id="{{ $order->id }}" data-type="customer" title="Chỉnh sửa"
                                     class="c-btn-edit btn-edit-information_customer"><i
                                            class="fa fa-pencil-square-o"></i></a></span>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="form-group">
                                <div class="col-sm-6 text-left">Họ tên:</div>
                                <div class="col-sm-6">{{$order->customer->name}}</div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 text-left">Email:</div>
                                <div class="col-sm-6">{{$order->customer->email}}</div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 text-left">Điện thoại:</div>
                                <div class="col-sm-6">{{$order->customer->phone}}</div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 text-left">Địa chỉ:</div>
                                <div class="col-sm-6">{{$order->customer->address}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8 block">
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="col-xs-12 title">THÔNG TIN GIAO HÀNG
                            <span><a data-id="{{ $order->id }}" data-type="shipping" title="Chỉnh sửa"
                                     class="c-btn-edit btn-edit-information_shipping"><i
                                            class="fa fa-pencil-square-o"></i></a></span>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="form-group">
                                <div class="col-sm-6 text-left">Tên người nhận:</div>
                                <div class="col-sm-6">{{$order->receiver_name}}</div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 text-left">Điện thoại người nhận:</div>
                                <div class="col-sm-6">{{$order->receiver_phone}}</div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 text-left">Địa chỉ nhận hàng:</div>
                                <div class="col-sm-6">{{$order->receiver_address}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-edit-customer_customer" role="dialog" class="modal fade" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" class="close">×</button>
                        <div class="modal-title"><h4 class="modal-title">Chỉnh sửa thông tin<span class="name-type">
                                    giao hàng</span></h4></div>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-sm-2 control-label">Họ tên<strong class="required">*</strong></div>
                                    <div class="col-sm-10"><input name="name" placeholder="Họ tên" value="{{$order->customer->name}}"
                                                                  class="form-control"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 control-label">Email<strong class="required">*</strong></div>
                                    <div class="col-sm-10"><input name="email" placeholder="Email" value="{{$order->customer->email}}"
                                                                  class="form-control inputmark-email"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 control-label">Số điện thoại</div>
                                    <div class="col-sm-10"><input name="phone" type="number" placeholder="Số điện thoại"
                                                                  value="{{$order->customer->phone}}"
                                                                  class="form-control inputmark-phone"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 control-label">Địa chỉ</div>
                                    <div class="col-sm-10"><input name="address" placeholder="Địa chỉ"
                                                                  value="{{$order->customer->address}}" class="form-control"></div>
                                </div>
                                <div class="order_fee hidden"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-default">Hủy</button>
                        <button class="btn btn-primary btn-update" data-type="shipping" data-id="{{ $order->customer_id }}">Cập nhật</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="btn-edit-information_shipping" role="dialog" class="modal fade" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" class="close">×</button>
                        <div class="modal-title"><h4 class="modal-title">Chỉnh sửa thông tin<span class="name-type">
                                    giao hàng</span></h4></div>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-sm-2 control-label">Họ tên<strong class="required">*</strong></div>
                                    <div class="col-sm-10"><input name="receiver_name" placeholder="Họ tên" value="{{$order->receiver_name}}"
                                                                  class="form-control"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 control-label">Số điện thoại</div>
                                    <div class="col-sm-10"><input name="receiver_phone" type="number" placeholder="Số điện thoại"
                                                                  value="{{$order->receiver_phone}}"
                                                                  class="form-control inputmark-phone"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 control-label">Địa chỉ</div>
                                    <div class="col-sm-10"><input name="receiver_address" placeholder="Địa chỉ"
                                                                  value="{{$order->receiver_address}}" class="form-control"></div>
                                </div>
                                <div class="order_fee hidden"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-default">Hủy</button>
                        <button class="btn btn-primary btn-update" data-type="shipping" data-id="{{ $order->id }}">Cập nhật</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('.btn-edit-information_customer').on('click', function () {
            var modal = $('#modal-edit-customer_customer');
            modal.modal('show');
        })
        $('#modal-edit-customer_customer .btn-update').click(function() {
            var id = $(this).data('id');
            var type = $(this).data('type');
            $('input').removeClass('error');
            var data = {};
            data.id = id;
            data.name = $('input[name="name"]').val();
            data.phone = $('input[name="phone"]').val();
            data.email = $('input[name="email"]').val();
            data.address = $('input[name="address"]').val();

            if(!data.name) {
                toastr.error('Chưa nhập họ tên khách hàng');
                $('input[name="name"]').addClass('error');
                return;
            }
            if(!data.email) {
                toastr.error('Chưa nhập email khách hàng');
                $('input[name="email"]').addClass('error');
                return;
            }

            if (!data.phone) {
                toastr.error('Chưa nhập số điện thoại khách hàng');
                $('input[name="phone"]').addClass('error');
                return;
            }
            if (!data.address) {
                toastr.error('Chưa nhập địa chỉ khách hàng');
                $('input[name="address"]').addClass('error');
                return;
            }
            if(type='customer') {
                updateCustomer(data);
            }
        })

        function updateCustomer(data) {
        $.ajax({
           type: 'POST',
           url: '/admin/customer/updateInfo',
            data: data,
            success: function(json) {
                $(document).find('.disabled').removeClass('disabled');
                if(!json.code) {
                    console.dir(json);
                    $('#modal-edit-customer').modal('hide');
                    toastr.success('Cập nhật thành công');
                    // reloadPage();
                    }
                    // else if (json.code == -1) {
                //     toastr.error('Khách hàng đã tồn tại');
                // } else if (json.code == -4) {
                //     toastr.error(json.message);
                // } else toastr.error('Có lỗi xảy ra, xin vui lòng thử lại');
            }
        });
        }

        $('.btn-edit-information_shipping').on('click', function () {
            var modal = $('#modal-edit-customer_customer');
            modal.modal('show');
        })
        $('#btn-edit-information_shipping .btn-update').click(function() {
            var id = $(this).data('id');
            var type = $(this).data('type');
            $('input').removeClass('error');
            var data = {};
            data.id = id;
            data.receiver_name = $('input[name="receiver_name"]').val();
            data.receiver_phone = $('input[name="receiver_phone"]').val();
            data.receiver_address = $('input[name="receiver_address"]').val();

            if(!data.receiver_phone) {
                toastr.error('Chưa nhập họ tên người nhận');
                $('input[name="receiver_name"]').addClass('error');
                return;
            }
            if(!data.receiver_phone) {
                toastr.error('Chưa nhập số điện thoại người nhận');
                $('input[name="receiver_phone"]').addClass('error');
                return;
            }
            if (!data.receiver_address) {
                toastr.error('Chưa nhập địa chỉ người nhận');
                $('input[name="receiver_address"]').addClass('error');
                return;
            }
            if(type=shipping) {
                updateShipping(data);
            }
        })

        function updateShipping(data) {
            $.ajax({
                type: 'POST',
                url: '/admin/order/updateInfo',
                data: data,
                success: function(json) {
                    $(document).find('.disabled').removeClass('disabled');
                    if(!json.code) {
                        console.dir(json);
                        $('#modal-edit-customer').modal('hide');
                        toastr.success('Cập nhật thành công');
                        // reloadPage();
                    }
                    // else if (json.code == -1) {
                    //     toastr.error('Khách hàng đã tồn tại');
                    // } else if (json.code == -4) {
                    //     toastr.error(json.message);
                    // } else toastr.error('Có lỗi xảy ra, xin vui lòng thử lại');
                }
            });
        }

        $('.btn-edit-information_shipping').on('click', function () {
            var modal = $('#btn-edit-information_shipping');
            modal.modal('show');
        })
        $("[data-toggle=tooltip]").tooltip();
        $('select[name=order_status]').on('change', function () {
            var id = $(this).data('id');
            var status = $(this).val();
            var params = {
                type: 'POST',
                url: '/admin/changeStatusOrder',
                data: {ids: [id], status: status},
                success: function (result) {
                    result = $.parseJSON(result);
                    if (result.code == 0) {
                        alert("Thành công!")
                    }
                }
            }
            $.ajax(params);
        })
    </script>
@endsection
