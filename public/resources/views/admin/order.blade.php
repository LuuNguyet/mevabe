@extends('admin.layout')
@section('header') Đơn hàng
@endsection
@section('content')
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="box box-info">
      <div class="form-horizontal form-product">
        <div class="box-title clearfix">
          <div class="pull-left title-tab">Đơn hàng
            @if ($page_name == 'new')
              <span class="label label-success">Mới</span>
            @elseif ($page_name == 'confirmed')
              <span class="label label-default">Đã xác nhận</span>
            @elseif ($page_name == 'paid')
              <span class="label label-primary">Đã thanh toán</span></a>
            @elseif ($page_name == 'cancel')
              <span class="label label-danger">Bị hủy</span>
            @else
              <span class="label label-info">Tất cả</span>
            @endif

             ({{$current_items}} trong tổng số {{$total_item}} đơn hàng)
          </div>
        </div>
        <div class="box-body">
          <div class="box-body">
            <div class="tab-content">
              <div class="form-group">
                <div class="col-lg-4 col-md-6">
                <div class="dataTables_length" id="DataTables_Table_0_length"><label>Hiển thị <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> dữ liệu</label></div>
                </div>
                <div class="col-lg-8 col-md-6">
                  <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Tìm kiếm: <input type="search" class="form-control input-sm" placeholder="Nhập từ khóa cần tìm..." aria-controls="DataTables_Table_0"></label></div>
                </div>
                <div class="col-lg-2 col-md-6">
                  <strong>Chỉ hiện</strong>
                </div>
                <div class="col-lg-10 col-md-6">
                  <a href="/admin/order_all?sort=only-new"><span class="label label-success">Mới</span> </a> |
                  <a href="/admin/order_all?sort=only-confirmed"><span class="label label-default">Đã xác nhận</span></a> |
                  <a href="/admin/order_all?sort=only-paid"><span class="label label-primary">Đã thanh toán</span></a> |
                  <a href="/admin/order_all?sort=only-cancel"><span class="label label-danger">Bị hủy</span></a> |
                  <a href="/admin/order_all"><span class="label label-info">Tất cả</span> </a> |
                </div>
              </div>
              <div class="form-group" id="actions" style="display: none">
                <div class="col-lg-2 col-md-6">
                  <strong>Chuyển tất cả thành </strong>
                </div>
                <div class="col-lg-10 col-md-6">
                  <a href="#" class="action_name" data-value="new"><span class="label label-success">Mới</span> </a> |
                  <a href="#" class="action_name" data-value="confirmed"><span class="label label-default">Đã xác nhận</span></a> |
                  <a href="#" class="action_name" data-value="paid"><span class="label label-primary">Đã thanh toán</span></a> |
                  <a href="#" class="action_name" data-value="cancel"><span class="label label-danger">Bị hủy</span></a> |
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="table-responsive">


              <table id="mytable" class="table table-bordred table-striped">
                <thead>
                  <th><input type="checkbox" id="checkall" /></th>
                  <th>#id</th>
                  <th>Khách hàng</th>
                  <th>Tổng giá trị</th>
                  <th>Phương thức</th>
                  <th>Trạng thái</th>
                  <th>Ngày đặt hàng</th>
                  <th>Sửa</th>
                </thead>
                <tbody>
                  @foreach ($orders as $order)
                  <tr class="item" id="{{$order->id}}">
                    <td><input type="checkbox" value="{{$order->id}}" class="checkthis action" /></td>
                    <td><a href="/admin/order?id={{$order->id}}">{{$order->id}}</a></td>
                    <td> <a href="/admin/order?id={{$order->id}}">{{$order->customer->name}}</a> </td>
                    <td>{{Helper::formatMoney($order->total)}} VNĐ</td>
                    <td><label class="label label-info">{{$order->payment_method}}</label></td>
                    <td class="status">
                      @if ($order->order_status == 'new')
                        <span class="label label-success">Mới</span>
                      @elseif ($order->order_status == 'confirmed')
                        <span class="label label-default">Đã xác nhận</span>
                      @elseif ($order->order_status == 'paid')
                        <span class="label label-primary">Đã thanh toán</span></a>
                      @elseif ($order->order_status == 'cancel')
                        <span class="label label-danger">Bị hủy</span>
                      @endif
                    </td>
                    <td>{{$order->created_at}}</td>
                    <td>
                      <p data-placement="top" data-toggle="tooltip" title="Edit">
                        <button class="btn btn-show-edit btn-primary btn-xs" data-title="Edit"  data-id="{{$order->id}}" >
                          <span class="glyphicon glyphicon-pencil"></span></button></p>
                    </td>
                  </tr>
                  @endforeach
                </tbody>

              </table>

              <div class="clearfix"></div>
              <ul class="pagination pull-right">
                <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                @for ($i=1; $i <= $total_page; $i++)
                  <li class="
                  @if ($page == $i)
                    active
                  @endif
                  "><a href="/admin/order_all?page={{$i}}&perpage={{$perpage}}">{{$i}}</a></li>
                @endfor
                <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
              </ul>

            </div>

          </div>


          <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                  <h4 class="modal-title custom_align" id="Heading">Sửa sơ bộ</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    <div class="col-lg-4 col-md-12 control-label">Tổng tiền<strong class="required">*</strong></div>
                    <div class="col-lg-8 col-md-12">
                      <input name="total" placeholder="Tên nhóm sản phẩm" class="settings form-control title text-overflow-title">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-lg-4 col-md-12 control-label">Trạng thái<strong class="required">*</strong></div>
                    <div class="col-lg-8 col-md-12">
                      <select class="form-control" name="order_status">
                        <option value="new">Mới</option>
                        <option value="confirmed">Đã xác nhận</option>
                        <option value="paid">Đã thanh toán</option>
                        <option value="cancel">Bị hủy</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="modal-footer ">
                  <button type="button" class="btn btn-primary btn-lg btn-quick-edit" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

  $(document).ready(function() {
    $("#mytable #checkall").click(function() {
      if ($("#mytable #checkall").is(':checked')) {
        $("#mytable input[type=checkbox]").each(function() {
          $(this).prop("checked", true);
        });
      } else {
        $("#mytable input[type=checkbox]").each(function() {
          $(this).prop("checked", false);
        });
      }
    });
    $("[data-toggle=tooltip]").tooltip();
    $('.btn-show-edit').on('click', function(e) {
      var id = $(this).data('id');
      $.ajax({type:'post',url:'/admin/getOrderById',data: {id: id}, success: function(result) {
        result = $.parseJSON(result);
        $('input[name=total]').val(result.data.total);
        $('select option[value=' + result.data.order_status + ']').attr('selected', 'selected');
        $('#edit').attr('data-id', id);
        $('#edit').modal('show');
      }})
    })
    $('.btn-quick-edit').on('click', function(e) {
      e.preventDefault();
      var id =   $('#edit').data('id');
      var total = $('input[name=total]').val();
      var status = $('select[name=order_status]').val();
      $.ajax({type:'POST',url:'/admin/quickUpdateOrder', data: {id: id, total:total, order_status: status}, success: function(result) {
        result = $.parseJSON(result);
        if (result.code == 0) {
          window.location.reload(true);
        }
      }})
    })
    $('input.action, #checkall').on('click', function(e) {
      var action = false;
      $.each($('input.action'), function(i, e) {
        if ($(e).prop('checked')) {
          action = true;
          return;
        }
      })
      if (action) {
        $('#actions').css('display', 'block');
      } else {
        $('#actions').css('display', 'none');
      }
    })
    $('.action_name').on('click', function(e) {
      e.preventDefault();
      var status = $(this).data('value');
      var ids = $("input.action:checkbox:checked").map(function(){
        return $(this).val();
      }).get();
      $.ajax({url: '/admin/changeStatusOrder', type:"POST", data: {ids: ids, status: status}, success: function(result) {
        if ($.parseJSON(result).code == 0) {
          console.dir($.parseJSON(result));
          window.location.reload(true);
        }
      }})
    })
  });
</script>
@endsection
