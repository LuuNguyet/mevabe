<div class="dv-builder-full">
    <div class="dv-builder logo_partner">
        <div class="title">
            <h3><span>THƯƠNG HIỆU NỔI BẬT</span></h3>
        </div>
        <div class="dv-module-content">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                    <div class="dv-item-module ">
                        <div id="carousel0" class="owl-carousel carousel_partner">
                            @for ($i=1; $i < 5; $i++)
                                <div class="item text-center">
                                    <img src="/uploads/{{Helper::getMeta('brand_'.$i)}}" alt="RedBull"
                                         class="img-responsive" style="width: 195px; height: 63px; margin: 0 49px;"/>
                                </div>
                            @endfor
                        </div>
                        <script type="text/javascript">
                            $('#carousel0').owlCarousel({
                                items: 4,
                                autoPlay: false,
                                navigation: true,
                                pagination: false,
                                itemsDesktop: [1199, 3],
                                itemsDesktopSmall: [979, 3],
                                itemsTablet: [767, 2],
                                itemsMobile: [480, 1],
                                navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>